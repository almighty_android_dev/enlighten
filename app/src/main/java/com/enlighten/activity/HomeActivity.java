package com.enlighten.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;

import com.enlighten.R;
import com.enlighten.adapter.SlidePagerAdapter;
import com.enlighten.databinding.ActivityHomeBinding;
import com.enlighten.fragment.FeedFragment;
import com.enlighten.fragment.MainFragment;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    AppCompatActivity activity;
    ActivityHomeBinding binding;
    PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        activity = this;
        binding = DataBindingUtil.setContentView( activity, R.layout.activity_home );

        List<Fragment> list = new ArrayList<>();
        list.add( new MainFragment());
        list.add( new FeedFragment());

        pagerAdapter = new SlidePagerAdapter( getSupportFragmentManager(), list );
        binding.viewPager.setAdapter( pagerAdapter );
        binding.viewPager.setCurrentItem( 1 );

    }
}