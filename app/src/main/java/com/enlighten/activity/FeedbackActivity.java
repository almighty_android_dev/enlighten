
package com.enlighten.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;

import com.enlighten.R;
import com.enlighten.databinding.ActivityFeedbackBinding;
import com.enlighten.utils.Constants;
import com.enlighten.utils.RetrofitHelper;
import com.enlighten.utils.StoreUserData;
import com.enlighten.utils.Utils;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class FeedbackActivity extends AppCompatActivity {

    AppCompatActivity activity;
    ActivityFeedbackBinding binding;
    StoreUserData storeUserData;
    int rating = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        activity = this;
        storeUserData = new StoreUserData( activity );
        binding = DataBindingUtil.setContentView( activity, R.layout.activity_feedback );
        binding.back.setOnClickListener( v -> finish() );

        binding.s1.setOnClickListener( v -> {
            changeImage( 1 );
            rating = 1;
            binding.s1.setImageResource( R.drawable.s1s );
        } );
        binding.s2.setOnClickListener( v -> {
            changeImage( 2 );
            rating = 2;
            binding.s2.setImageResource( R.drawable.s2s );
        } );
        binding.s3.setOnClickListener( v -> {
            changeImage( 3 );
            rating = 3;
            binding.s3.setImageResource( R.drawable.s3s );
        } );
        binding.s4.setOnClickListener( v -> {
            changeImage( 4 );
            rating = 4;
            binding.s4.setImageResource( R.drawable.s4s );
        } );
        binding.s5.setOnClickListener( v -> {
            changeImage( 5 );
            rating = 5;
            binding.s5.setImageResource( R.drawable.s5s );
        } );

        if (getIntent().getStringExtra( "Activity" ).equalsIgnoreCase( "feedback" )){
            binding.tvTitle.setText( "Feedback" );
            binding.layoutFeedback.setVisibility( View.VISIBLE );
            binding.svStaticPage.setVisibility( View.GONE );
            binding.layoutContactUs.setVisibility( View.GONE );
        }else if (getIntent().getStringExtra( "Activity" ).equalsIgnoreCase( "terms" )){
            binding.tvTitle.setText( "Terms & Conditions" );
            binding.layoutFeedback.setVisibility( View.GONE );
            binding.layoutContactUs.setVisibility( View.GONE );
            binding.svStaticPage.setVisibility( View.VISIBLE );
            staticPage( "terms" );
        }else if (getIntent().getStringExtra( "Activity" ).equalsIgnoreCase( "policy" )){
            binding.tvTitle.setText( "Privacy Policy" );
            binding.layoutFeedback.setVisibility( View.GONE );
            binding.layoutContactUs.setVisibility( View.GONE );
            binding.svStaticPage.setVisibility( View.VISIBLE );
            staticPage( "policy" );
        }else if (getIntent().getStringExtra( "Activity" ).equalsIgnoreCase( "contact" )){
            binding.tvTitle.setText( "Contact Us" );
            binding.layoutFeedback.setVisibility( View.GONE );
            binding.svStaticPage.setVisibility( View.GONE );
            binding.layoutContactUs.setVisibility( View.VISIBLE );
            getContactDetail();
        }

        binding.tvEmail1.setOnClickListener( v -> {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto",binding.tvEmail1.getText().toString(), null));
            startActivity(emailIntent);
        } );

        binding.tvEmail2.setOnClickListener( v -> {

            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto",binding.tvEmail2.getText().toString(), null));
            startActivity(emailIntent);
        } );

        binding.btnSendFeedback.setOnClickListener( v -> {
            if (rating == 0){
                Utils.showCustomAlert(activity,"Please Give Ratting");
            }else if(Utils.isEmpty(binding.etCommentBox)){
                Utils.showCustomAlert(activity,"Please Add Your Comment");
            }else
                sendFeedBack();
            new Handler().postDelayed( () -> {
                binding.etCommentBox.getText().clear();
                changeImage(0);
                rating = 0;
            }, 1500 );

        } );
    }

    public void changeImage(int clickType) {
        binding.s1.setImageResource( R.drawable.s1 );
        binding.s2.setImageResource( R.drawable.s2 );
        binding.s3.setImageResource( R.drawable.s3 );
        binding.s4.setImageResource( R.drawable.s4 );
        binding.s5.setImageResource( R.drawable.s5 );

        if (clickType == 1) {
            binding.s1.setImageResource( R.drawable.s1s );
        } else if (clickType == 2) {
            binding.s1.setImageResource( R.drawable.s1s );
            binding.s2.setImageResource( R.drawable.s2s );
        } else if (clickType == 3) {
            binding.s1.setImageResource( R.drawable.s1s );
            binding.s2.setImageResource( R.drawable.s2s );
            binding.s3.setImageResource( R.drawable.s3s );
        } else if (clickType == 4) {
            binding.s1.setImageResource( R.drawable.s1s );
            binding.s2.setImageResource( R.drawable.s2s );
            binding.s3.setImageResource( R.drawable.s3s );
            binding.s4.setImageResource( R.drawable.s4s );
        } else if (clickType == 5) {
            binding.s1.setImageResource( R.drawable.s1s );
            binding.s2.setImageResource( R.drawable.s2s );
            binding.s3.setImageResource( R.drawable.s3s );
            binding.s4.setImageResource( R.drawable.s4s );
            binding.s5.setImageResource( R.drawable.s5s );
        }
    }

    private void sendFeedBack() {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;

        call = retrofitHelper.api().sendFeedBack(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN ),
                ""+rating,
                binding.etCommentBox.getText().toString().trim()
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "FEEDBACK_RESPONSE", "" + response );

                    JSONObject jsonObject = new JSONObject( response );

                    if (jsonObject.getInt( "status" ) == 1) {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                        JSONObject data = jsonObject.getJSONObject( "responsedata" );

                    } else {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
                if (code == -1) {
                    Utils.showCustomAlert( activity, error );
                }
                Log.e( "Login Error", error );
            }
        } );
    }

    //TODO : GET POLICY FROM WEB
    private void staticPage(String pageName) {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call = retrofitHelper.api().staticPage( pageName );
        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "POLICY", "" + response );
                    JSONObject jsonObject = new JSONObject( response );
                    if (jsonObject.getInt( "status" ) == 1) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject( "responsedata" );
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            binding.tvTerms.setText( Html.fromHtml( jsonObject1.getString( "content" ), Html.FROM_HTML_MODE_COMPACT ) );
                        } else {
                            binding.tvTerms.setText( Html.fromHtml( jsonObject1.getString( "content" ) ) );
                        }
                    }
                } catch (IOException | NullPointerException | JsonSyntaxException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                try {
                    Utils.dismissProgress();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } );
    }

    private void getContactDetail() {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;

        call = retrofitHelper.api().getContactDetail(
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "GUEST_LOGIN_RESPONSE", "" + response );

                    JSONObject jsonObject = new JSONObject( response );

                    if (jsonObject.getInt( "status" ) == 1) {
                        JSONObject data = jsonObject.getJSONObject( "responsedata" );
                        binding.tvEmail1.setText( data.getString( "email1" ) );
                        binding.tvEmail2.setText( data.getString( "email2" ) );
                    } else {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                    }
                } catch (IOException | NullPointerException | JsonSyntaxException | JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
                if (code == -1) {
                    Utils.showCustomAlert( activity, error );
                }
                Log.e( "Login Error", error );
            }
        } );
    }
}