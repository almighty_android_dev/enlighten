package com.enlighten.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.enlighten.R;
import com.enlighten.adapter.VerticalViewPager;
import com.enlighten.adapter.VerticlePagerAdapter3;
import com.enlighten.databinding.ActivityDemoScroll3Binding;
import com.enlighten.pojo.MotivationPojo;
import com.enlighten.utils.Constants;
import com.enlighten.utils.RetrofitHelper;
import com.enlighten.utils.StoreUserData;
import com.enlighten.utils.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class MotivationActivity extends AppCompatActivity{

    AppCompatActivity activity;
    ActivityDemoScroll3Binding binding;
    StoreUserData storeUserData;
    MotivationPojo data;
    VerticlePagerAdapter3 pagerAdapter;
    final ArrayList<MotivationPojo.ResponseData> responsedata = new ArrayList<>();
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        activity = this;
        storeUserData = new StoreUserData( activity );
        binding = DataBindingUtil.setContentView(activity,R.layout.activity_demo_scroll3 );
        pagerAdapter = new VerticlePagerAdapter3( activity,responsedata);
        binding.vPager3.setAdapter(pagerAdapter);

        MobileAds.initialize(activity, initializationStatus -> {} );
        interstitialAd = new InterstitialAd(activity);
        interstitialAd.setAdUnitId("ca-app-pub-4958310727087459/4348480957");

        interstitialAd.setAdListener(
                new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        if (interstitialAd != null && interstitialAd.isLoaded()) {

                        } else {
                            if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                                AdRequest adRequest = new AdRequest.Builder().build();
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        String error =
                                String.format(
                                        "domain: %s, code: %d, message: %s",
                                        loadAdError.getDomain(), loadAdError.getCode(), loadAdError.getMessage());

                        Log.i( "Add_Fail", "onAdFailedToLoad: "+loadAdError );
                    }

                    @Override
                    public void onAdClosed() {
                        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                            AdRequest adRequest = new AdRequest.Builder().build();
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                });

        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder().build();
            interstitialAd.loadAd(adRequest);
        }

        binding.vPager3.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position % data.ad_count == 0 && data.ad_type.equalsIgnoreCase( "google" )) {
                    interstitialAd.show();
                }

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        motivation();
    }

    private void motivation() {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;

        call = retrofitHelper.api().motivation(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN )
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "MOTIVATION_RESPONSE", "" + response );

                    Reader reader = new StringReader(response);
                    Gson gson = new GsonBuilder()
                            .excludeFieldsWithModifiers( Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .serializeNulls()
                            .create();

                    data = gson.fromJson(reader, MotivationPojo.class);

                    if (data.status == 1){
                        responsedata.clear();
                        responsedata.addAll( data.responsedata );
                        pagerAdapter.notifyDataSetChanged();
                    }else {
                        Utils.showCustomAlert( activity, data.message );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
            }
        } );
    }

}