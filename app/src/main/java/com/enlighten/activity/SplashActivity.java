package com.enlighten.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.enlighten.R;
import com.enlighten.databinding.ActivitySplashBinding;
import com.enlighten.utils.Constants;
import com.enlighten.utils.RetrofitHelper;
import com.enlighten.utils.StoreUserData;
import com.enlighten.utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;


public class SplashActivity extends AppCompatActivity {

    public FirebaseRemoteConfig firebaseRemoteConfig;
    AppCompatActivity activity;
    ActivitySplashBinding binding;
    StoreUserData storeUserData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        activity = this;
        FirebaseApp.initializeApp( activity );
        binding = DataBindingUtil.setContentView( activity, R.layout.activity_splash );
        storeUserData = new StoreUserData( activity );
        Glide.with( activity ).load( R.drawable.splash ).into( binding.ivSplash );


        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        firebaseRemoteConfig.setConfigSettingsAsync( new FirebaseRemoteConfigSettings.Builder().setMinimumFetchIntervalInSeconds( 3600L ).build() );

        //FCM
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( activity, instanceIdResult -> {
            Log.i( "FCM_TOKEN", instanceIdResult.getToken() );
            storeUserData.setString( Constants.USER_FCM, "" + instanceIdResult.getToken() );

            if (storeUserData.getString( Constants.USER_ID ).length() >0){
                new Handler().postDelayed( () -> {
                    startActivity( new Intent( activity, HomeActivity.class ) );
                    finish();
                }, 2000 );
            }else {
                guestLogin( storeUserData.getString( Constants.TOKEN ) );
            }
        } );
    }

    private void guestLogin(String fcmTOKEN) {
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;

        call = retrofitHelper.api().guestLogin(
                fcmTOKEN
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "GUEST_LOGIN_RESPONSE", "" + response );

                    JSONObject jsonObject = new JSONObject( response );

                    if (jsonObject.getInt( "status" ) == 1) {
                        JSONObject data = jsonObject.getJSONObject( "responsedata" );
                        storeUserData.setString( Constants.USER_ID, data.getString( "user_id" ) );
                        storeUserData.setString( Constants.TOKEN, data.getString( "token" ) );
                        storeUserData.setString( Constants.NOTIFICATION, data.getString( "notification_status" ) );
                        storeUserData.setString( Constants.USER_TYPE, data.getString( "user_type" ) );

                        new Handler().postDelayed( () -> {
                            startActivity( new Intent( activity, HomeActivity.class ) );
                            finish();
                        }, 2000 );

                    } else {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException | JSONException e) {
                    Log.i( "Login_EXE", "" + e );
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
                if (code == -1) {
                    Utils.showCustomAlert( activity, error);
                }
                Log.e( "Login Error", error );
            }
        } );
    }
}