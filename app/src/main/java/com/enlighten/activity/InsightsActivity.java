package com.enlighten.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;

import com.enlighten.R;
import com.enlighten.adapter.InsightsPagerAdapter;
import com.enlighten.databinding.ActivityInsightsBinding;
import com.enlighten.pojo.FeedPojo;
import com.enlighten.pojo.InsightsPojo;
import com.enlighten.utils.Constants;
import com.enlighten.utils.RetrofitHelper;
import com.enlighten.utils.StoreUserData;
import com.enlighten.utils.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;


public class InsightsActivity extends AppCompatActivity {

    AppCompatActivity activity;
    ActivityInsightsBinding binding;
    StoreUserData storeUserData;
    InsightsPagerAdapter PagerAdapter;
    final ArrayList<InsightsPojo.Responsedata> responsedata = new ArrayList<>();
    private InterstitialAd interstitialAd;
    InsightsPojo data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        activity = this;
        storeUserData = new StoreUserData( activity );
        binding = DataBindingUtil.setContentView( activity, R.layout.activity_insights );
        getInsights();

        MobileAds.initialize(activity, initializationStatus -> {} );
        interstitialAd = new InterstitialAd(activity);
        interstitialAd.setAdUnitId("ca-app-pub-4958310727087459/4348480957");

        interstitialAd.setAdListener(
                new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        if (interstitialAd != null && interstitialAd.isLoaded()) {

                        } else {
                            //Toast.makeText(this, "Ad did not load", Toast.LENGTH_SHORT).show();
                            if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                                AdRequest adRequest = new AdRequest.Builder().build();
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        String error =
                                String.format(
                                        "domain: %s, code: %d, message: %s",
                                        loadAdError.getDomain(), loadAdError.getCode(), loadAdError.getMessage());

                        Log.i( "Add_Fail", "onAdFailedToLoad: "+loadAdError );
                    }

                    @Override
                    public void onAdClosed() {
                        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                            AdRequest adRequest = new AdRequest.Builder().build();
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                });
        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder().build();
            interstitialAd.loadAd(adRequest);
        }


    }

    private void getInsights() {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();

        Call<ResponseBody> call;

        call = retrofitHelper.api().getInsights(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN )
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "INSIGHTS_RESPONSE", "" + response );

                    Reader reader = new StringReader( response );

                    Gson gson = new GsonBuilder()
                            .excludeFieldsWithModifiers( Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC )
                            .serializeNulls()
                            .create();

                    InsightsPojo data = gson.fromJson( reader, InsightsPojo.class );

                    if (data.status == 1) {
                        responsedata.clear();
                        responsedata.addAll( data.responsedata );

                        PagerAdapter = new InsightsPagerAdapter( activity, responsedata );
                        binding.viewPager.setAdapter( PagerAdapter );

                        binding.indicator.setViewPager( binding.viewPager );

                        PagerAdapter.notifyDataSetChanged();

                        binding.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageSelected(int position) {
                                if (position % data.ad_count == 0 && data.ad_type.equalsIgnoreCase( "google" )) {
                                    interstitialAd.show();
                                }

                            }

                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {
                            }
                        });

                    } else {
                        Utils.showCustomAlert( activity, data.message );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
            }
        } );
    }
}