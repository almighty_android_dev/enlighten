package com.enlighten.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.enlighten.R;
import com.enlighten.adapter.OpinionPagerAdapter;
import com.enlighten.databinding.ActivityMyOpinionBinding;
import com.enlighten.pojo.OpinionPojo;
import com.enlighten.utils.Constants;
import com.enlighten.utils.RetrofitHelper;
import com.enlighten.utils.StoreUserData;
import com.enlighten.utils.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class MyOpinion extends AppCompatActivity {

    AppCompatActivity activity;
    ActivityMyOpinionBinding binding;
    StoreUserData storeUserData;
    OpinionPagerAdapter pagerAdapter;
    OpinionPojo data;
    final ArrayList<OpinionPojo.Responsedata> responsedata = new ArrayList<>();
    private MyOpinion.ItemClickListener itemClickListener;
    TextToSpeech tts;
    int answerStatus,opinionId;
    private InterstitialAd interstitialAd;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        activity = this;
        binding = DataBindingUtil.setContentView( activity, R.layout.activity_my_opinion );
        storeUserData = new StoreUserData( activity );
        getOpinion();

        MobileAds.initialize(activity, initializationStatus -> {} );
        interstitialAd = new InterstitialAd(activity);
        interstitialAd.setAdUnitId("ca-app-pub-4958310727087459/4348480957");

        interstitialAd.setAdListener(
                new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        if (interstitialAd != null && interstitialAd.isLoaded()) {

                        } else {
                            //Toast.makeText(this, "Ad did not load", Toast.LENGTH_SHORT).show();
                            if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                                AdRequest adRequest = new AdRequest.Builder().build();
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        String error =
                                String.format(
                                        "domain: %s, code: %d, message: %s",
                                        loadAdError.getDomain(), loadAdError.getCode(), loadAdError.getMessage());

                        Log.i( "Add_Fail", "onAdFailedToLoad: "+loadAdError );
                    }

                    @Override
                    public void onAdClosed() {
                        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                            AdRequest adRequest = new AdRequest.Builder().build();
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                });
        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder().build();
            interstitialAd.loadAd(adRequest);
        }

        binding.viewPagerOpinion.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position % data.ad_count == 0 && data.ad_type.equalsIgnoreCase( "google" )) {
                    interstitialAd.show();
                }

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        itemClickListener = new ItemClickListener() {
            @Override
            public void onClick(String message) {
                tts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
            }

            @Override
            public void ButtonYes(int answer ,int j) {
                answerStatus = answer;
                opinionId = j;
                opinionAnswer();
            }

            @Override
            public void ButtonNo(int answer, int i) {
                answerStatus = answer;
                opinionId = i;
                opinionAnswer();
            }
        };
        pagerAdapter = new OpinionPagerAdapter( activity, responsedata, itemClickListener );
        binding.viewPagerOpinion.setAdapter(pagerAdapter);
        tts=new TextToSpeech( activity.getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR){
                    tts.setLanguage( Locale.US );
                }
            }
        } );
        binding.viewPagerOpinion.setOnScrollChangeListener( new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                tts.stop();
            }
        } );

    }
    private void opinionAnswer() {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;
        call = retrofitHelper.api().opinionAnswer(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN ),
                ""+opinionId,
                ""+answerStatus
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "ANSWER_RESPONSE", "" + response );
                    JSONObject jsonObject = new JSONObject( response );
                    if (jsonObject.getInt( "status" ) == 1) {
                        JSONObject data = jsonObject.getJSONObject( "responsedata" );
                    } else {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                    }
                } catch (IOException | NullPointerException | JsonSyntaxException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
                if (code == -1) {
                    Utils.showCustomAlert( activity, error );
                }
                Log.e( "Login Error", error );
            }
        } );
    }

    public interface ItemClickListener{
        void onClick(String message);

        void ButtonYes(int answer ,int j);

        void ButtonNo(int answer, int i);
    }

    public void onPause(){
        if(tts!=null){
            tts.stop();
            tts.shutdown();
        }
        super.onPause();
    }

    private void getOpinion() {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();

        Call<ResponseBody> call;

        call = retrofitHelper.api().getOpinion(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN )
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "OPINION_RESPONSE", "" + response );

                    Reader reader = new StringReader(response);

                    Gson gson = new GsonBuilder()
                            .excludeFieldsWithModifiers( Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                            .serializeNulls()
                            .create();

                    data = gson.fromJson(reader, OpinionPojo.class);

                    if (data.status == 1){
                        responsedata.clear();
                        responsedata.addAll( data.responsedata );
                        pagerAdapter.notifyDataSetChanged();
                    }else {
                        Utils.showCustomAlert( activity, data.message );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
            }
        } );
    }

}