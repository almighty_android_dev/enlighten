package com.enlighten.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.enlighten.R;
import com.enlighten.adapter.FeedPagerAdapter;
import com.enlighten.databinding.ActivityMyFeedBinding;
import com.enlighten.pojo.FeedPojo;
import com.enlighten.utils.Constants;
import com.enlighten.utils.RetrofitHelper;
import com.enlighten.utils.StoreUserData;
import com.enlighten.utils.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class MyFeedActivity extends AppCompatActivity {

    AppCompatActivity activity;
    private MyFeedActivity.ItemClickListener itemClickListener;
    TextToSpeech tts;
    ActivityMyFeedBinding binding;
    FeedPagerAdapter pagerAdapter;
    StoreUserData storeUserData;
    int bookmark;
    private InterstitialAd interstitialAd;
    FeedPojo data;
    final ArrayList<FeedPojo.Responsedata> responsedata = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        activity = this;
        binding = DataBindingUtil.setContentView( activity, R.layout.activity_my_feed );
        storeUserData = new StoreUserData( activity );

        MobileAds.initialize(activity, initializationStatus -> {} );
        interstitialAd = new InterstitialAd(activity);
        interstitialAd.setAdUnitId("ca-app-pub-4958310727087459/4348480957");

        interstitialAd.setAdListener(
                new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        if (interstitialAd != null && interstitialAd.isLoaded()) {

                        } else {
                            //Toast.makeText(this, "Ad did not load", Toast.LENGTH_SHORT).show();
                            if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                                AdRequest adRequest = new AdRequest.Builder().build();
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        String error =
                                String.format(
                                        "domain: %s, code: %d, message: %s",
                                        loadAdError.getDomain(), loadAdError.getCode(), loadAdError.getMessage());

                        Log.i( "Add_Fail", "onAdFailedToLoad: "+loadAdError );
                    }

                    @Override
                    public void onAdClosed() {
                        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                            AdRequest adRequest = new AdRequest.Builder().build();
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                });
        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder().build();
            interstitialAd.loadAd(adRequest);
        }

        binding.viewPagerFeed.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position % data.ad_count == 0 && data.ad_type.equalsIgnoreCase( "google" )) {
                    interstitialAd.show();
                }

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        tts = new TextToSpeech( activity.getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR) {
                    tts.setLanguage( Locale.US );
                }
            }
        } );


        if (storeUserData.getString( Constants.USER_ID ).length() > 0) {
            if (Objects.requireNonNull( getIntent().getStringExtra( "TYPE" ) ).equalsIgnoreCase( "MY_FEED" )) {
                getFeed();
            } else {
                getNews( getIntent().getStringExtra( "TYPE" ) );
            }
        } else {
            guestLogin( storeUserData.getString( Constants.TOKEN ) );
        }

        itemClickListener = new MyFeedActivity.ItemClickListener() {
            @Override
            public void onClick(String message, ImageView imageView) {
                tts.speak( message, TextToSpeech.QUEUE_FLUSH, null );
            }

            @Override
            public void llBookMark(int id) {
                bookmark = id;
                markBookMark();
            }

            @Override
            public void Refresh(String refresh) {

                if (refresh.equalsIgnoreCase( "MY_FEED" )){
                    getFeed();
                }else {
                    getNews( refresh );
                }

            }
        };

        pagerAdapter = new FeedPagerAdapter( activity, responsedata, itemClickListener, getIntent().getStringExtra( "TYPE" ) );
        binding.viewPagerFeed.setAdapter( pagerAdapter );

        binding.viewPagerFeed.addOnPageChangeListener( new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                markNewsRead(""+data.responsedata.get( position ).id);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        } );

        binding.viewPagerFeed.setOnScrollChangeListener( new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                tts.stop();
                pagerAdapter.notifyDataSetChanged();
            }
        } );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public interface ItemClickListener {
        public void onClick(String message, ImageView imageView);

        public void llBookMark(int id);

        public  void Refresh(String refresh);
    }

    public void onPause() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onPause();
    }

    private void getFeed() {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();

        Call<ResponseBody> call;

        call = retrofitHelper.api().getFeed(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN )
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "FEED_RESPONSE", "" + response );

                    Reader reader = new StringReader( response );

                    Gson gson = new GsonBuilder().excludeFieldsWithModifiers( Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC ).serializeNulls().create();

                    data = gson.fromJson( reader, FeedPojo.class );

                    if (data.status == 1) {
                        responsedata.clear();
                        responsedata.addAll( data.responsedata );
                        pagerAdapter.notifyDataSetChanged();

                        //push_Counter();

                    } else {
                        Utils.showCustomAlert( activity, data.message );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
            }
        } );
    }

    private void markBookMark() {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();

        Call<ResponseBody> call;

        call = retrofitHelper.api().markBookMark(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN ),
                "" + bookmark
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "BOOKMARK_RESPONSE", "" + response );

                    JSONObject jsonObject = new JSONObject( response );

                    if (jsonObject.getInt( "status" ) == 1) {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                        JSONObject data = jsonObject.getJSONObject( "responsedata" );


                    } else {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
                if (code == -1) {
                    Utils.showCustomAlert( activity, error );
                }
                Log.e( "Login Error", error );
            }
        } );
    }

    private void markNewsRead(String id) {
        RetrofitHelper retrofitHelper = new RetrofitHelper();

        Call<ResponseBody> call;

        call = retrofitHelper.api().markNewsRead(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN ),
                "" + id
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "MARK_READ", "" + response );




                } catch (IOException | NullPointerException | JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
                if (code == -1) {
                    Utils.showCustomAlert( activity, error );
                }
                Log.e( "Login Error", error );
            }
        } );
    }

    private void getNews(String type) {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();

        Call<ResponseBody> call;

        call = retrofitHelper.api().getNews(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN ),
                type
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "GET_NEWS_RESPONSE", "" + response );

                    Reader reader = new StringReader( response );

                    Gson gson = new GsonBuilder()
                            .excludeFieldsWithModifiers( Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC )
                            .serializeNulls()
                            .create();

                    data = gson.fromJson( reader, FeedPojo.class );

                    if (data.status == 1) {
                        responsedata.clear();
                        responsedata.addAll( data.responsedata );
                        pagerAdapter.notifyDataSetChanged();

                        //push_Counter();

                    } else {
                        Utils.showCustomAlert( activity, data.message );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
            }
        } );
    }

    private void guestLogin(String fcmTOKEN) {
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;

        call = retrofitHelper.api().guestLogin(
                fcmTOKEN
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "GUEST_LOGIN_RESPONSE", "" + response );

                    JSONObject jsonObject = new JSONObject( response );

                    if (jsonObject.getInt( "status" ) == 1) {
                        JSONObject data = jsonObject.getJSONObject( "responsedata" );
                        storeUserData.setString( Constants.USER_ID, data.getString( "user_id" ) );
                        storeUserData.setString( Constants.TOKEN, data.getString( "token" ) );
                        storeUserData.setString( Constants.NOTIFICATION, data.getString( "notification_status" ) );
                        storeUserData.setString( Constants.USER_TYPE, data.getString( "user_type" ) );

                        if (Objects.requireNonNull( getIntent().getStringExtra( "TYPE" ) ).equalsIgnoreCase( "MY_FEED" )) {
                            getFeed();
                        } else {
                            getNews( getIntent().getStringExtra( "TYPE" ) );
                        }

                    } else {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException | JSONException e) {
                    Log.i( "Login_EXE", "" + e );
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
                if (code == -1) {
                    Utils.showCustomAlert( activity, error );
                }
                Log.e( "Login Error", error );
            }
        } );
    }

    private void push_Counter() {
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;

        call = retrofitHelper.api().push_Counter(
                storeUserData.getString( Constants.USER_ID),
                storeUserData.getString( Constants.TOKEN),
                storeUserData.getString( Constants.USER_FCM)
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "PUSH_COUNTER_RESPONSE", "" + response );


                } catch (IOException | NullPointerException | JsonSyntaxException  e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {

            }
        } );
    }
}