package com.enlighten.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.enlighten.R;
import com.enlighten.databinding.ActivityLoginBinding;
import com.enlighten.utils.Constants;
import com.enlighten.utils.RetrofitHelper;
import com.enlighten.utils.StoreUserData;
import com.enlighten.utils.Utils;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    AppCompatActivity activity;
    ActivityLoginBinding binding;
    StoreUserData storeUserData;
    int RC_SIGN_IN = 0;
    GoogleSignInClient mGoogleSignInClient;
    String personName;
    String personGivenName;
    String personEmail;
    String personId;
    Uri personPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        activity = this;
        storeUserData = new StoreUserData( activity );
        binding = DataBindingUtil.setContentView( activity, R.layout.activity_login );
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder( GoogleSignInOptions.DEFAULT_SIGN_IN )
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient( this, gso );

        binding.btnLogin.setOnClickListener( view -> {
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult( signInIntent, RC_SIGN_IN );
        } );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data );

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount( this );
            if (acct != null) {
                personName = acct.getDisplayName();
                personGivenName = acct.getGivenName();
                personEmail = acct.getEmail();
                personId = acct.getId();
                personPhoto = acct.getPhotoUrl();
                googleLogin();
            }
        }
    }

    private void handleSignInResult() {
        storeUserData.setString( Constants.LOGIN_WITH_GOOGLE,"YES" );
        finish();
    }

    private void googleLogin() {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;

        call = retrofitHelper.api().googleLogin(
                ""+personName,
                ""+personEmail,
                ""+personPhoto,
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.USER_FCM )
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "LOGIN_RESPONSE", "" + response );

                    JSONObject jsonObject = new JSONObject( response );

                    if (jsonObject.getInt( "status" ) == 1) {
                        JSONObject data = jsonObject.getJSONObject( "responsedata" );

                        storeUserData.setString( Constants.USER_NAME, data.getString( "full_name" ) );
                        storeUserData.setString( Constants.USER_ID, data.getString( "user_id" ) );
                        storeUserData.setString( Constants.TOKEN, data.getString( "token" ) );
                        storeUserData.setString( Constants.NOTIFICATION, data.getString( "notification_status" ) );
                        storeUserData.setString( Constants.USER_IMAGE, data.getString( "image" ) );

                        handleSignInResult();
                    } else {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException | JSONException e) {
                    Log.i( "LOGIN_EXE", "" + e );
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {

            }
        } );
    }
}