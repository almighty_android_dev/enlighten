package com.enlighten.pojo;

import java.util.ArrayList;

public class OpinionPojo {

    public int status;
    public String message;
    public int ad_count;
    public String ad_type;

    public ArrayList<Responsedata> responsedata = new ArrayList<>(  );

    public  class Responsedata {
        public int id;
        public String media_type;
        public String image;
        public String video;
        public String title;
        public String content;
        public String bottom_line;
        //public String source_name;
        public String source_link;
        public String poll_que;
        public String is_ans_given;
        public String yes_per;
        public String no_per;
        public String is_ad;
        public String ad_type;
    }
}
