package com.enlighten.pojo;

import java.util.ArrayList;

public class InsightsPojo {

    public int status;
    public String message;
    public int ad_count;
    public String ad_type;

    public ArrayList<Responsedata> responsedata = new ArrayList<>(  );

    public  class Responsedata {
        public String id;
        public String media_type;
        public String image;
        public String video;
        public String source_link;
        public String is_ad;
    }
}
