package com.enlighten.pojo;

import java.util.ArrayList;

public class SuggestedTopicPojo {

    public int status;
    public String message;
    public SuggestedTopicPojo.Responsedata responsedata;

    public  class Responsedata {

        public ArrayList<GetTopic> topics = new ArrayList<>();
        public SuggestedTopicPojo.Responsedata.Links links;

        public class GetTopic {
            public int id;
            public String name;
            public String image;
        }

        public class Links {
            public String fb_url;
            public String insta_url;
            public String twitter_url;
        }
    }
}
