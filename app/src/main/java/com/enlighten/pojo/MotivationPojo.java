package com.enlighten.pojo;

import java.util.ArrayList;

public class MotivationPojo {

    public int status;
    public String message;
    public int ad_count;
    public String ad_type;

    public ArrayList<ResponseData> responsedata = new ArrayList<>(  );

    public  class ResponseData {
        public String id;
        public String media_type;
        public String image;
        public String source_link;
        public String quote;
        public String is_ad;
    }
}
