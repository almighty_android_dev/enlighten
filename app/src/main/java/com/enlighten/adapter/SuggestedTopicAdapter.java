package com.enlighten.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.enlighten.activity.MyFeedActivity;
import com.enlighten.databinding.SuggestedTopicRowBinding;
import com.enlighten.pojo.SuggestedTopicPojo;
import com.enlighten.R;

import java.util.ArrayList;

public class SuggestedTopicAdapter extends RecyclerView.Adapter<SuggestedTopicAdapter.ViewHolder> {

    Activity activity;
    private ArrayList<SuggestedTopicPojo.Responsedata.GetTopic> arrayList;
    //  private HomeFragment.ItemClickListener itemClickListener;

    public SuggestedTopicAdapter(Activity activity, ArrayList<SuggestedTopicPojo.Responsedata.GetTopic> arrayList/*, HomeFragment.ItemClickListener itemClickListener*/) {
        this.activity = activity;
        this.arrayList = arrayList;
        //  this.itemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.suggested_topic_row, parent, false );
        return new ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final SuggestedTopicPojo.Responsedata.GetTopic pojo = arrayList.get( position );

        holder.binding.tvSuggestedTopic.setText( pojo.name );
        Glide.with( activity ).load( pojo.image ).into( holder.binding.ivSuggestedTopic );

        holder.itemView.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity( new Intent( activity, MyFeedActivity.class )
                        .putExtra( "TYPE", ""+ pojo.id ) );
            }
        } );
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        SuggestedTopicRowBinding binding;

        public ViewHolder(View view) {
            super( view );
            binding = DataBindingUtil.bind( itemView );
        }
    }
}