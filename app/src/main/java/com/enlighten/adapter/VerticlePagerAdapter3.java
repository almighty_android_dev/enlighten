package com.enlighten.adapter;
//user_id=59&auth_token=evrCNuWgKWjM8Q3X

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.enlighten.R;
import com.enlighten.activity.MotivationActivity;
import com.enlighten.activity.MyFeedActivity;
import com.enlighten.activity.MyOpinion;
import com.enlighten.pojo.MotivationPojo;
import com.google.android.gms.ads.InterstitialAd;
import com.potyvideo.library.AndExoPlayerView;

import java.util.ArrayList;
import java.util.HashMap;

public class VerticlePagerAdapter3 extends PagerAdapter {

    AppCompatActivity activity;
    LayoutInflater mLayoutInflater2;
    private ArrayList<MotivationPojo.ResponseData> list;
    private InterstitialAd interstitialAd;
    private CountDownTimer countDownTimer;
    private Button retryButton;
    private boolean gameIsInProgress;
    private long timerMilliseconds;
    String add_count , add_type;

    public VerticlePagerAdapter3(AppCompatActivity activity, ArrayList<MotivationPojo.ResponseData> mResources) {
        this.activity = activity;
        mLayoutInflater2 = (LayoutInflater) activity.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        this.list = mResources;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final MotivationPojo.ResponseData pojo = list.get( position );
        View itemView = mLayoutInflater2.inflate( R.layout.motivation_slider, container, false );

        ImageView imageView = itemView.findViewById( R.id.imageViewMotivation );
        ImageView imageViewAdd = itemView.findViewById( R.id.addMotivation );
        ImageView back = itemView.findViewById( R.id.backMoti );
        TextView tvMotivation = itemView.findViewById( R.id.tvMotivation );
        LinearLayout llQuate = itemView.findViewById( R.id.llQuate );
        LinearLayout llvideo = itemView.findViewById( R.id.llVideoMotivation );
        LinearLayout llMain = itemView.findViewById( R.id.llMainMotivation );
        FrameLayout frameLayout = itemView.findViewById( R.id.frameLayoutMotivation );
        AndExoPlayerView videoViewAdd = itemView.findViewById( R.id.videoMotivationAdd );

        imageViewAdd.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get( position ).source_link));
                activity.startActivity(browserIntent);
            }
        } );

        videoViewAdd.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get( position ).source_link));
                activity.startActivity(browserIntent);
            }
        } );

        frameLayout.setOnTouchListener(new OnSwipeTouchListener(activity) {
            public void onSwipeLeft() {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get( position ).source_link));
                activity.startActivity(browserIntent);
            }

            public void onSwipeRight() {
                activity.finish();
            }

        });

        if (pojo.media_type.equalsIgnoreCase( "image" )) {

            if (pojo.is_ad.equalsIgnoreCase( "1" )) {
                imageView.setVisibility( View.GONE );
                imageViewAdd.setVisibility( View.VISIBLE );
                tvMotivation.setVisibility( View.GONE );

                Glide.with( activity )
                        .load( pojo.image )
                        .into( imageViewAdd );
            } else {
                imageView.setVisibility( View.VISIBLE );
                tvMotivation.setVisibility( View.GONE );
                llQuate.setVisibility( View.GONE );

                Glide.with( activity )
                        .load( pojo.image )
                        .into( imageView );
            }

        } else {
            imageView.setVisibility( View.GONE );
            tvMotivation.setVisibility( View.VISIBLE );

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvMotivation.setText( Html.fromHtml( pojo.quote, Html.FROM_HTML_MODE_COMPACT ) );
            } else {
                tvMotivation.setText( Html.fromHtml( pojo.quote ) );
            }
        }

        if (pojo.is_ad.equalsIgnoreCase( "1" ) && pojo.media_type.equalsIgnoreCase( "video" )) {

            llvideo.setVisibility( View.VISIBLE );
            videoViewAdd.setVisibility( View.VISIBLE );
            HashMap<String, String> extraHeaders = new HashMap<>();
            videoViewAdd.setSource( pojo.quote );
        }

        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        } );

        container.addView( itemView );
        return itemView;
    }




   /* private void createTimer(final long milliseconds) {
        // Create the game timer, which counts down to the end of the level
        // and shows the "retry" button.
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }


        countDownTimer = new CountDownTimer(milliseconds, 50) {
            @Override
            public void onTick(long millisUnitFinished) {
                timerMilliseconds = millisUnitFinished;
            }

            @Override
            public void onFinish() {
                gameIsInProgress = false;
                retryButton.setVisibility(View.VISIBLE);
            }
        };
    }

    private void showInterstitial() {
        // Show the ad if it's ready. Otherwise toast and restart the game.
        if (interstitialAd != null && interstitialAd.isLoaded()) {
            interstitialAd.show();
        } else {
            Toast.makeText(activity, "Ad did not load", Toast.LENGTH_SHORT).show();
            startGame();
        }
    }

    private void startGame() {
        // Request a new ad if one isn't already loaded, hide the button, and kick off the timer.
        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder().build();
            interstitialAd.loadAd(adRequest);
        }

    }

    private void resumeGame(long milliseconds) {
        gameIsInProgress = true;
        timerMilliseconds = milliseconds;
        createTimer(milliseconds);
        countDownTimer.start();
    }*/




    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView( (FrameLayout) object );
    }
}