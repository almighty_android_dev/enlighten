package com.enlighten.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.enlighten.R;
import com.enlighten.activity.MyOpinion;
import com.enlighten.pojo.OpinionPojo;
import com.jaedongchicken.ytplayer.YoutubePlayerView;
import com.potyvideo.library.AndExoPlayerView;

import java.util.ArrayList;
import java.util.HashMap;

public class OpinionPagerAdapter extends PagerAdapter {

    AppCompatActivity activity;
    LayoutInflater mLayoutInflater2;
    public MyOpinion.ItemClickListener itemClickListener;
    private ArrayList<OpinionPojo.Responsedata> list;
    YoutubePlayerView youtubePlayerView;
    public boolean play = false;

    public OpinionPagerAdapter(AppCompatActivity activity, ArrayList<OpinionPojo.Responsedata> mResources, MyOpinion.ItemClickListener itemClickListener) {
        this.activity = activity;
        this.itemClickListener = itemClickListener;
        mLayoutInflater2 = (LayoutInflater) activity.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        this.list = mResources;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final OpinionPojo.Responsedata pojo = list.get( position );
        View itemView = mLayoutInflater2.inflate( R.layout.opinion_slider, container, false );

        ImageView imageView = itemView.findViewById( R.id.imageViewOpinion );
        ImageView imageViewAdd = itemView.findViewById( R.id.ivAddOpinion );
        ImageView btnshare = itemView.findViewById( R.id.ivShareOpinion );
        AndExoPlayerView videoViewAdd = itemView.findViewById( R.id.videoOpinionAdd );
        TextView Title = itemView.findViewById( R.id.tvTitleOpinion );
        TextView Content = itemView.findViewById( R.id.tvContent );
        TextView Bottemline = itemView.findViewById( R.id.tvBottemLine );
        TextView PollQue = itemView.findViewById( R.id.tvPollQuestion );
        TextView YesPer = itemView.findViewById( R.id.tvYesPer );
        TextView NoPer = itemView.findViewById( R.id.tvNoPer );
        TextView perNo = itemView.findViewById( R.id.perNo );
        TextView perYes = itemView.findViewById( R.id.perYes );
        TextView ButtonYes = itemView.findViewById( R.id.btnYes );
        TextView ButtonNo = itemView.findViewById( R.id.btnNo );
        LinearLayout Buttons = itemView.findViewById( R.id.rating );
        FrameLayout rattingGiven = itemView.findViewById( R.id.rating2 );
        FrameLayout frameLayout = itemView.findViewById( R.id.frameLayoutOpinion );
        LinearLayout llvideo = itemView.findViewById( R.id.llVideoOpinion );
        LinearLayout llNewsOpinion = itemView.findViewById( R.id.llNewsOpinion );
        ProgressBar progress = itemView.findViewById( R.id.activeProgress );

        if(youtubePlayerView != null) {
            youtubePlayerView.pause();
        }

        imageViewAdd.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get( position ).source_link));
                activity.startActivity(browserIntent);
            }
        } );

        videoViewAdd.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get( position ).source_link));
                activity.startActivity(browserIntent);
            }
        } );

        progress.setProgress( Integer.parseInt( pojo.yes_per ) );

        frameLayout.setOnTouchListener(new OnSwipeTouchListener(activity) {
            public void onSwipeLeft() {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pojo.source_link));
                activity.startActivity(browserIntent);
            }

            public void onSwipeRight() {
                activity.finish();
            }
        });

        YesPer.setText(""+pojo.yes_per );
        NoPer.setText(""+ pojo.no_per );

        Bottemline.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( pojo.source_link ) );
                activity.startActivity( browserIntent );
            }
        } );

        if (YesPer.getText().toString().equalsIgnoreCase( "100" )){
            perNo.setVisibility( View.GONE );
        } else if (NoPer.getText().toString().equalsIgnoreCase( "100" )){
            perYes.setVisibility( View.GONE );
        }

        if (YesPer.getText().toString().equalsIgnoreCase( "0" )){
            YesPer.setVisibility( View.GONE );
        } else if (NoPer.getText().toString().equalsIgnoreCase( "0" )){
            NoPer.setVisibility( View.GONE );
        }

        if (pojo.media_type.equalsIgnoreCase( "image" )) {

            if (pojo.is_ad.equalsIgnoreCase( "1" )) {
                llNewsOpinion.setVisibility( View.GONE );
                imageViewAdd.setVisibility( View.VISIBLE );
                Glide.with( activity )
                        .load( pojo.image )
                        .into( imageViewAdd );
            } else {
                imageView.setVisibility( View.VISIBLE );
                Glide.with( activity )
                        .load( pojo.image )
                        .into( imageView );
            }

        } else {

            if (pojo.is_ad.equalsIgnoreCase( "1" )) {
                llvideo.setVisibility( View.VISIBLE );
                videoViewAdd.setVisibility( View.VISIBLE );
                llNewsOpinion.setVisibility( View.GONE );

                HashMap<String, String> extraHeaders = new HashMap<>();
                videoViewAdd.setSource( pojo.video );
            } else {
              //  youTubePlayerView.setVisibility( View.VISIBLE );
                HashMap<String, String> extraHeaders = new HashMap<>();
                String Id;
                if (pojo.video.length() > 0 ){
                    String currentString = pojo.video;
                    Id = currentString.substring( currentString.length() - 11 );
                }else {
                    Id = "2n4n2lRuYTk";
                }
                youtubePlayerView = (YoutubePlayerView) itemView.findViewById(R.id.youtubePlayerViewOpinion);
                youtubePlayerView.initialize(Id, new YoutubePlayerView.YouTubeListener() {

                    @Override
                    public void onReady() {
                    }

                    @Override
                    public void onStateChange(YoutubePlayerView.STATE state) {
                    }

                    @Override
                    public void onPlaybackQualityChange(String arg) {
                    }

                    @Override
                    public void onPlaybackRateChange(String arg) {
                    }

                    @Override
                    public void onError(String error) {
                    }

                    @Override
                    public void onApiChange(String arg) {
                    }

                    @Override
                    public void onCurrentSecond(double second) {
                        // currentTime callback
                    }

                    @Override
                    public void onDuration(double duration) {
                        // total duration
                    }

                    @Override
                    public void logs(String log) {
                        // javascript debug log. you don't need to use it.
                    }
                });

            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Title.setText( Html.fromHtml( pojo.title, Html.FROM_HTML_MODE_COMPACT ) );
        } else {
            Title.setText( Html.fromHtml( pojo.title ) );
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Content.setText( Html.fromHtml( pojo.content, Html.FROM_HTML_MODE_COMPACT ) );
        } else {
            Content.setText( Html.fromHtml( pojo.content ) );
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Bottemline.setText( Html.fromHtml( pojo.bottom_line, Html.FROM_HTML_MODE_COMPACT ) );
        } else {
            Bottemline.setText( Html.fromHtml( pojo.bottom_line ) );
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            PollQue.setText( Html.fromHtml( pojo.poll_que, Html.FROM_HTML_MODE_COMPACT ) );
        } else {
            PollQue.setText( Html.fromHtml( pojo.poll_que ) );
        }

        ButtonYes.setOnClickListener( v -> {
            int j = list.get( position ).id;
            int answer = 1;
            Buttons.setVisibility( View.GONE );
            rattingGiven.setVisibility( View.VISIBLE );
            itemClickListener.ButtonYes( answer, j );
        } );

        ButtonNo.setOnClickListener( v -> {
            int i = list.get( position ).id;
            int answer = 0;
           Buttons.setVisibility( View.GONE );
            rattingGiven.setVisibility( View.VISIBLE );
            itemClickListener.ButtonNo( answer, i );
        } );

        if (pojo.is_ans_given.equalsIgnoreCase( "1" )){
            rattingGiven.setVisibility( View.VISIBLE );
            Buttons.setVisibility( View.GONE );
        } else {
            Buttons.setVisibility( View.VISIBLE );
            rattingGiven.setVisibility( View.GONE);
        }

        ImageView speech = (ImageView) itemView.findViewById( R.id.textToSpeechO );

        speech.setOnClickListener( view -> {
            //videoView.pausePlayer();
            videoViewAdd.pausePlayer();

            if (!play) {
                play = true;
                speech.setImageResource( R.drawable.pause );
                itemClickListener.onClick( Content.getText().toString() );
            } else {
                play = false;
                speech.setImageResource( R.drawable.play );
                itemClickListener.onClick( "" );
            }
        } );

        btnshare.setOnClickListener( v -> {

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Enlighten");
            String shareMessage = Title.getText().toString() + "\n \n" + Content.getText().toString() + "\nCheck out Enlighten on Play store. I found it best for news, feed, daily motivations and latest inshorts.\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + activity.getPackageName() +"\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            activity.startActivity(Intent.createChooser(shareIntent, "choose one"));
        } );

        container.addView( itemView );
        return itemView;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView( (FrameLayout) object );
    }
}
