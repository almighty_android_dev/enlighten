package com.enlighten.adapter;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.enlighten.R;
import com.enlighten.pojo.InsightsPojo;
import com.enlighten.utils.Utils;
import com.potyvideo.library.AndExoPlayerView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class InsightsPagerAdapter extends PagerAdapter {

    AppCompatActivity activity;
    LayoutInflater mLayoutInflater2;
    private ArrayList<InsightsPojo.Responsedata> list;
    long downlaodId;
    String fileName;
    long downloadIdImage, downloadIdVideo;
    BroadcastReceiver onComplete, onCompleteVideo;
    InsightsPojo.Responsedata pojo;

    public InsightsPagerAdapter(AppCompatActivity activity, ArrayList<InsightsPojo.Responsedata> mResources) {
        this.activity = activity;
        mLayoutInflater2 = (LayoutInflater) activity.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        this.list = mResources;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        InsightsPojo.Responsedata pojo = list.get( position );
        View itemView = mLayoutInflater2.inflate( R.layout.insight_row, container, false );

        ImageView imageView = itemView.findViewById( R.id.imageInsights );
        ImageView imageViewAdd = itemView.findViewById( R.id.imageViewAdd );
        ImageView btnShare = itemView.findViewById( R.id.btnShareInsights );
        AndExoPlayerView videoView = itemView.findViewById( R.id.videoInsights );
        AndExoPlayerView videoViewAdd = itemView.findViewById( R.id.videoInsightsAdd );
        LinearLayout llvideo = itemView.findViewById( R.id.llVideo );
        FrameLayout frameLayout = itemView.findViewById( R.id.frameLayoutInsights );

        imageViewAdd.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get( position ).source_link));
                activity.startActivity(browserIntent);
            }
        } );

        videoViewAdd.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get( position ).source_link));
                activity.startActivity(browserIntent);
            }
        } );

        frameLayout.setOnTouchListener( new OnSwipeTouchListener( activity ) {
            public void onSwipeRight() {
                activity.finish();
            }
        } );

        if (pojo.media_type.equalsIgnoreCase( "image" )) {

            if (pojo.is_ad.equalsIgnoreCase( "1" )) {
                imageViewAdd.setVisibility( View.VISIBLE );
                Glide.with( activity )
                        .load( pojo.image )
                        .into( imageViewAdd );
            } else {
                imageView.setVisibility( View.VISIBLE );
                Glide.with( activity )
                        .load( pojo.image )
                        .into( imageView );
            }

        } else {

            if (pojo.is_ad.equalsIgnoreCase( "1" )) {
                llvideo.setVisibility( View.VISIBLE );
                videoViewAdd.setVisibility( View.VISIBLE );
                HashMap<String, String> extraHeaders = new HashMap<>();
                videoViewAdd.setSource( pojo.video );
            } else {
                videoView.setVisibility( View.VISIBLE );
                HashMap<String, String> extraHeaders = new HashMap<>();
                videoView.setSource( pojo.video );

            }
        }


        btnShare.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (list.get( position ).media_type.equalsIgnoreCase( "image" )) {


                    Bitmap bm = ((android.graphics.drawable.BitmapDrawable) imageView.getDrawable()).getBitmap();
                    try {
                        java.io.File file = new java.io.File( activity.getExternalCacheDir() + "/image.jpg" );
                        java.io.OutputStream out = new java.io.FileOutputStream( file );
                        bm.compress( Bitmap.CompressFormat.JPEG, 100, out );
                        out.flush();
                        out.close();
                        Intent iten = new Intent( android.content.Intent.ACTION_SEND );
                        iten.setType( "*/*" );
                        iten.putExtra( Intent.EXTRA_STREAM, Uri.fromFile( new java.io.File( activity.getExternalCacheDir() + "/image.jpg" ) ) );
                        iten.putExtra( Intent.EXTRA_TEXT, "\nCheck out Enlighten on Play store. I found it best for news, feed, daily motivations and latest inshorts.\n" + "https://play.google.com/store/apps/details?id=" + activity.getPackageName() + "\n\n" );
                        activity.startActivity( Intent.createChooser( iten, "Send image" ) );
                    } catch (Exception e) {
                        Log.i( "Error", "" + e.toString() );
                        String bitmapPath = MediaStore.Images.Media.insertImage( activity.getContentResolver(), bm, "some title", null );
                        Uri bitmapUri = Uri.parse( bitmapPath );
                        Intent shareIntent = new Intent( Intent.ACTION_SEND );
                        shareIntent.setType( "image/jpeg" );
                        shareIntent.putExtra( Intent.EXTRA_STREAM, bitmapUri );
                        shareIntent.putExtra( Intent.EXTRA_TEXT, "\nCheck out Enlighten on Play store. I found it best for news, feed, daily motivations and latest inshorts.\n" + "https://play.google.com/store/apps/details?id=" + activity.getPackageName() + "\n\n" );
                        activity.startActivity( Intent.createChooser( shareIntent, "Share via" ) );
                    }


                } else {

                    String videoFileName = Objects.requireNonNull( Uri.parse( list.get( position ).video ).getPath() ).substring( Objects.requireNonNull( Uri.parse( list.get( position ).video ).getPath() ).lastIndexOf( "/" ) + 1 );
                    File file = new File( Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS ), videoFileName );
                    try {
                        if (file.exists()) {


                            Uri imageUri = Uri.parse( String.valueOf( file ) );
                            Intent shareIntent = new Intent();
                            shareIntent.setAction( Intent.ACTION_SEND );
                            shareIntent.putExtra( Intent.EXTRA_STREAM, imageUri );
                            shareIntent.setType( "video/mp4" );
                            shareIntent.addFlags( Intent.FLAG_GRANT_READ_URI_PERMISSION );
                            activity.startActivity( Intent.createChooser( shareIntent, "share" ) );


                        } else {

                            videoBroadCast( videoFileName );

                            Utils.showProgress( activity );
                            activity.registerReceiver( onCompleteVideo, new IntentFilter( DownloadManager.ACTION_DOWNLOAD_COMPLETE ) );
                            DownloadManager.Request request = new DownloadManager.Request( Uri.parse( list.get( position ).video ) );
                            request.setDescription( "download" );
                            request.setTitle( videoFileName );
                            request.allowScanningByMediaScanner();
                            request.setNotificationVisibility( DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED );
                            request.setDestinationInExternalPublicDir( Environment.DIRECTORY_DOWNLOADS, "" + videoFileName );
                            DownloadManager manager = (DownloadManager) activity.getSystemService( Context.DOWNLOAD_SERVICE );
                            downloadIdVideo = Objects.requireNonNull( manager ).enqueue( request );

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } );

        container.addView( itemView );
        return itemView;

    }

    public void videoBroadCast(String videoPath) {
        onCompleteVideo = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                long id = intent.getLongExtra( DownloadManager.EXTRA_DOWNLOAD_ID, -1 );
                if (downloadIdVideo == id) {

                    Utils.dismissProgress();
                    try {
                        File file1 = new File( Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS ) + "/" + videoPath );
                        if (file1.exists()) {

                            Uri imageUri = Uri.parse( String.valueOf( file1 ) );
                            Intent shareIntent = new Intent();
                            shareIntent.setAction( Intent.ACTION_SEND );
                            shareIntent.putExtra( Intent.EXTRA_STREAM, imageUri );
                            shareIntent.setType( "video/mp4" );
                            shareIntent.addFlags( Intent.FLAG_GRANT_READ_URI_PERMISSION );
                            activity.startActivity( Intent.createChooser( shareIntent, "share" ) );


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.i( "EXCEPTION_VIDEO", "" + e );
                    }
                }
            }
        };
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView( (LinearLayout) object );
    }
}