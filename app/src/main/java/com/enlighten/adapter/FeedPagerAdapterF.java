package com.enlighten.adapter;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.enlighten.R;
import com.enlighten.fragment.FeedFragment;
import com.enlighten.pojo.FeedPojo;
import com.jaedongchicken.ytplayer.YoutubePlayerView;
import com.potyvideo.library.AndExoPlayerView;

import java.util.ArrayList;

import static com.enlighten.R.id.tvTitleFeed;

public class FeedPagerAdapterF extends PagerAdapter {

    FragmentActivity activity;
    LayoutInflater mLayoutInflater;
    public FeedFragment.ItemClickListener itemClickListener;
    private ArrayList<FeedPojo.Responsedata> list;
    String type;
    public boolean play = false;
    FeedPojo.Responsedata pojo;
    YoutubePlayerView youtubePlayerView;

    public FeedPagerAdapterF(FragmentActivity activity, ArrayList<FeedPojo.Responsedata> mResources, FeedFragment.ItemClickListener itemClickListener, String type) {
        this.activity = activity;
        this.itemClickListener = itemClickListener;
        mLayoutInflater = (LayoutInflater) activity.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        this.list = mResources;
        this.type = type;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate( R.layout.content_main, container, false );
        pojo = list.get( position );

        ImageView speech = itemView.findViewById( R.id.textToSpeechFeed );
        LinearLayout llMainLayout = itemView.findViewById( R.id.llMainLayout );
        LinearLayout llTopLayout = itemView.findViewById( R.id.llTopLayout );
        ImageView feedImage = itemView.findViewById( R.id.imageViewFeed );
        ImageView imageViewAdd = itemView.findViewById( R.id.ivAddFeed );
        ImageView imageMenu = itemView.findViewById( R.id.imgMenu );
        AndExoPlayerView videoAds = itemView.findViewById( R.id.videoAds );
        LinearLayout llVideoAds = itemView.findViewById( R.id.llVideoAds );
        LinearLayout llKnowMore = itemView.findViewById( R.id.llKnowMore );
        TextView title = itemView.findViewById( tvTitleFeed );
        TextView tvContentFeed = itemView.findViewById( R.id.tvContentFeed );
        TextView BottomLine = itemView.findViewById( R.id.tvBottomLineFeed );
        LinearLayout llMenu = itemView.findViewById( R.id.llMenu );
        FrameLayout frameLayout = itemView.findViewById( R.id.frameLayoutMyFeed );
        speech.setImageResource( R.drawable.play );

        if(youtubePlayerView != null) {
            youtubePlayerView.pause();
        }

        imageViewAdd.setOnClickListener( v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get( position ).source_link));
            activity.startActivity(browserIntent);
        } );

        videoAds.setOnClickListener( v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get( position ).source_link));
            activity.startActivity(browserIntent);
        } );

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)imageMenu.getLayoutParams();
        params.setMargins(0, 190, 0, 0);
        imageMenu.setLayoutParams(params);

        frameLayout.setOnTouchListener( new OnSwipeTouchListener( activity ) {
            public void onSwipeLeft() {
                Intent browserIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( list.get( position ).source_link  ) );
                activity.startActivity( browserIntent );
            }
        } );

        llKnowMore.setOnClickListener( v -> {
            Intent browserIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( list.get( position ).source_link ) );
            activity.startActivity( browserIntent );
        } );

        LinearLayout llBookmark = itemView.findViewById( R.id.llBookmark );
        LinearLayout llRefresh = itemView.findViewById( R.id.llRefreshBtn );

        llRefresh.setOnClickListener( v -> {
            llMenu.setVisibility( View.GONE );
            String refresh = type;
            itemClickListener.Refresh( refresh );
        } );

        if (type.equalsIgnoreCase( "MY_FEED" ) || type.equalsIgnoreCase( "bookmarked" )) {
            llBookmark.setVisibility( View.GONE );
        } else {
            llBookmark.setVisibility( View.VISIBLE );
        }

        if (pojo.is_ad == 1) {

            llMainLayout.setVisibility( View.GONE );
            llTopLayout.setVisibility( View.GONE );

            if (pojo.media_type.equalsIgnoreCase( "image" )) {

                imageViewAdd.setVisibility( View.VISIBLE );
                llVideoAds.setVisibility( View.GONE );

                Glide.with( activity )
                        .load( pojo.image )
                        .into( imageViewAdd );
            } else {
                imageViewAdd.setVisibility( View.GONE );
                llVideoAds.setVisibility( View.VISIBLE );
                videoAds.setSource( pojo.video );
            }

        } else {

            imageViewAdd.setVisibility( View.GONE );
            llVideoAds.setVisibility( View.GONE );
            llMainLayout.setVisibility( View.VISIBLE );
            llTopLayout.setVisibility( View.VISIBLE );

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                title.setText( Html.fromHtml( pojo.title, Html.FROM_HTML_MODE_COMPACT ) );
            } else {
                title.setText( Html.fromHtml( pojo.title ) );
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvContentFeed.setLinksClickable( true );
                tvContentFeed.setMovementMethod( LinkMovementMethod.getInstance() );
                tvContentFeed.setText( Html.fromHtml( pojo.content, Html.FROM_HTML_MODE_COMPACT ));
            } else {
                tvContentFeed.setText( Html.fromHtml( pojo.content ));
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                BottomLine.setText( Html.fromHtml( pojo.bottom_line, Html.FROM_HTML_MODE_COMPACT ) );
            } else {
                BottomLine.setText( Html.fromHtml( pojo.bottom_line ) );
            }

            if (pojo.media_type.equalsIgnoreCase( "image" )) {
                feedImage.setVisibility( View.VISIBLE );

                Glide.with( activity )
                        .load( pojo.image )
                        .into( feedImage );
            } else {
                feedImage.setVisibility( View.GONE );

                String Id;
                if (pojo.video.length() > 0 ){
                    String currentString = pojo.video;
                    Id = currentString.substring( currentString.length() - 11 );
                }else {
                    Id = "2n4n2lRuYTk";
                }
                youtubePlayerView = (YoutubePlayerView) itemView.findViewById(R.id.youtubePlayerView);
                youtubePlayerView.setVisibility( View.VISIBLE );
                youtubePlayerView.initialize(Id, new YoutubePlayerView.YouTubeListener() {

                    @Override
                    public void onReady() {
                    }

                    @Override
                    public void onStateChange(YoutubePlayerView.STATE state) {
                    }

                    @Override
                    public void onPlaybackQualityChange(String arg) {
                    }

                    @Override
                    public void onPlaybackRateChange(String arg) {
                    }

                    @Override
                    public void onError(String error) {
                    }

                    @Override
                    public void onApiChange(String arg) {
                    }

                    @Override
                    public void onCurrentSecond(double second) {
                    }

                    @Override
                    public void onDuration(double duration) {
                    }

                    @Override
                    public void logs(String log) {
                    }
                });

            }

            LinearLayout llShare = itemView.findViewById( R.id.llShareBtn );
            llShare.setOnClickListener( v -> {
                llMenu.setVisibility( View.GONE );

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Enlighten");
                String shareMessage = title.getText().toString() + "\n \n" + tvContentFeed.getText().toString() + "\nCheck out Enlighten on Play store. I found it best for news, feed, daily motivations and latest inshorts.\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + activity.getPackageName() +"\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                activity.startActivity(Intent.createChooser(shareIntent, "choose one"));

            } );

        }

        llBookmark.setOnClickListener( v -> {
            llMenu.setVisibility( View.GONE );
            int id = list.get( position ).id;
            itemClickListener.llBookMark( id );
        } );

        imageMenu.setOnClickListener( v -> {
            if (llMenu.getVisibility() == View.VISIBLE) {
                llMenu.setVisibility( View.INVISIBLE );
            } else {
                llMenu.setVisibility( View.VISIBLE );
            }
        } );

        speech.setOnClickListener( view -> {
            //youtubePlayerView.pause();
            videoAds.pausePlayer();

            if (!play) {
                play = true;
                speech.setImageResource( R.drawable.pause );
                itemClickListener.onClick( title.getText().toString() + tvContentFeed.getText().toString(), speech );
            } else {
                play = false;
                speech.setImageResource( R.drawable.play );
                itemClickListener.onClick( "", speech );
            }
        } );
        container.addView( itemView );
        return itemView;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView( (FrameLayout) object );

    }
}
