package com.enlighten.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.enlighten.R;
import com.enlighten.activity.HomeActivity;
import com.enlighten.utils.Constants;
import com.enlighten.utils.StoreUserData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    static int notificationId = 0;

    @Override
    public void onNewToken(String s) {
        super.onNewToken( s );
        new StoreUserData( this ).setString( Constants.USER_FCM, s );
        Log.i( "From: ", Constants.USER_FCM );
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        Log.d( TAG, "From: " + remoteMessage.getFrom() );
        try {
            if (remoteMessage.getData().size() > 0) {
                Log.d( TAG, "Message data payload ==> " + remoteMessage.getData().toString() );
                sendNotification( remoteMessage.getData().get( "message" ), remoteMessage.getData().get( "cmd" ) );
               // sendNotification( remoteMessage.getData().toString() );
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i( "NOTIFICTION_EXCEPTION", e + "" );
        }
    }

//    @Override
//    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
//        super.onMessageReceived( remoteMessage );
//        showNotification( remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody() );
//        Log.i( "NOTIFICTION_DATA", remoteMessage.getNotification().getBody());
//       // sendNotification( remoteMessage.getData().toString() );
//    }

    private void sendNotification(String message, String messageBody) {
        Intent intent;
        Log.i( "jhdfjsdjf==>", "main" );

        NotificationManager notificationManager = (NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );

        intent = new Intent( this, HomeActivity.class );
        intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
        PendingIntent pendingIntent = PendingIntent.getActivity( this, 0, intent, PendingIntent.FLAG_ONE_SHOT );

        String channelId = "signals";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        CharSequence channelName = "NewsApp";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel( channelId, channelName, importance );
            notificationChannel.enableLights( true );
            notificationChannel.setLightColor( Color.RED );
            notificationChannel.enableVibration( true );
            notificationChannel.setVibrationPattern( new long[]{100, 200} );
            notificationManager.createNotificationChannel( notificationChannel );
        }

//inboxStyle.setBigContentTitle(getResources().getString(R.string.app_name));
//inboxStyle.addLine(messageBody);
//Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder( this, channelId )
                .setSmallIcon( getNotificationIcon() )
                .setContentTitle( getResources().getString( R.string.app_name ) )
                .setContentText( messageBody )
                .setColor( ContextCompat.getColor( this, R.color.colorPrimaryDark ) )
//.setGroup(groupKey)
                .setAutoCancel( true )
                .setStyle( new NotificationCompat.BigTextStyle().bigText( messageBody ) )
                .setVibrate( new long[]{100, 200} )
//.setSound(defaultSoundUri)
//.setNumber(++notificationId)
//.setStyle(inboxStyle)
                .setContentIntent( pendingIntent );
        notificationId++;
        notificationManager.notify( 999, notificationBuilder.build() );
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }


    public void showNotification(String title, String message) {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder( this, "MyNotifications" )
                .setContentTitle( title )
                .setSmallIcon( android.R.mipmap.sym_def_app_icon )
                .setAutoCancel( true )
                .setContentText( message );
        NotificationManagerCompat manager = NotificationManagerCompat.from( this );
        manager.notify( 999, notificationBuilder.build() );

    }

}
