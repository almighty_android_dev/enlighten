package com.enlighten.controls;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.enlighten.R;

public class CustomAlertDialog extends Dialog {
    Context context;
    String msg;
    private TextView messageText,okText;

    public CustomAlertDialog(@NonNull Context context, String msg) {
        super(context, R.style.CustomDialogTheme);
        this.context = context;
        this.msg = msg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE);
        getWindow().addFlags( WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.custom_alert_dialog);
        setCancelable(false);

        messageText = findViewById(R.id.message);
        messageText.setText(msg);

        okText = findViewById(R.id.oKText);
        okText.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        } );

    }
}
