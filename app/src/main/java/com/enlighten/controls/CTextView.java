package controls;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.Gravity;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.enlighten.R;



public class CTextView extends AppCompatTextView {

    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public CTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context, attrs, 0);

    }

    public CTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context, attrs, defStyle);

    }

    private void applyCustomFont(Context context, AttributeSet attrs, int defStyle) {
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        setTypeface(selectTypeface(context, textStyle));
        if (getGravity() == 49 || getGravity() == Gravity.CENTER || getGravity() == Gravity.CENTER_HORIZONTAL || getGravity() == Gravity.CENTER_VERTICAL) {

        } else {
            setTextAlignment(TEXT_ALIGNMENT_VIEW_START);
            setTextDirection(TEXT_DIRECTION_LOCALE);
        }
    }

    private Typeface selectTypeface(Context context, int textStyle) {
        switch (textStyle) {
            case Typeface.BOLD:
                return Typeface.createFromAsset(context.getAssets(), "ms-pgothic.ttf");
            case Typeface.NORMAL:
                return Typeface.createFromAsset(context.getAssets(), "ms-pgothic.ttf");// regular
            default:
                return Typeface.createFromAsset(context.getAssets(), "ms-pgothic.ttf");// regular
        }
    }
}