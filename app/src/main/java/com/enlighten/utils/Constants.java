package com.enlighten.utils;

public class Constants {


    public static final String URL = "http://app.globalenlighten.com/";

    public static final String STRIPE_KEY = "pk_test_cOH61omzxr1KOycuCHvtZ8Jw00KO8hLqBW";
    public static int DONT_ALLOW_PERMISSION = 0;

    public static final String USER_NAME = "USER_NAME";
    public static final String USER_ID = "USER_ID";
    public static final String TOKEN = "TOKEN";
    public static final String NOTIFICATION = "NOTIFICATION";
    public static final String LOGIN_WITH_GOOGLE = "LOGIN_WITH_GOOGLE";
    public static final String USER_FCM = "USER_FCM";
    public static final String USER_TYPE = "USER_TYPE";
    public static final String USER_IMAGE = "USER_IMAGE";
    public static final String BOOKMARK = "BOOKMARK";

    public static int Next_Motivation = 0;
}