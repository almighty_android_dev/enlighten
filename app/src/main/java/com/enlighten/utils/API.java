package com.enlighten.utils;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API {

    String URLPrefix = "webservices/";

    @FormUrlEncoded
    @POST(URLPrefix + "login_with_google.php")
    Call<ResponseBody> googleLogin(
            @Field("full_name") String full_name,
            @Field("email") String email,
            @Field("image_url") String image_url,
            @Field("guest_user_id") String guest_user_id,
            @Field("device_token") String device_token
    );

    @GET(URLPrefix + "login_with_guest.php")
    Call<ResponseBody> guestLogin(
            @Query("device_token") String device_token
    );

    @GET(URLPrefix + "update_push_counter.php")
    Call<ResponseBody> push_Counter(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token,
            @Query("device_token") String device_token
    );

    @GET(URLPrefix + "get_opinions.php")
    Call<ResponseBody> getOpinion(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token
    );

    @GET(URLPrefix + "get_insights.php")
    Call<ResponseBody> getInsights(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token
    );

    @GET(URLPrefix + "get_daily_motivations.php")
    Call<ResponseBody> motivation(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token
    );

    @GET(URLPrefix + "get_topics.php")
    Call<ResponseBody> getTopic(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token
    );

    @GET(URLPrefix + "get_news.php")
    Call<ResponseBody> getNews(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token,
            @Query("type") String type
    );

    @GET(URLPrefix + "get_feed.php")
    Call<ResponseBody> getFeed(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token
    );

    @GET(URLPrefix + "send_feedback.php")
    Call<ResponseBody> sendFeedBack(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token,
            @Query("rating") String rating,
            @Query("comment") String comment
    );

    @GET(URLPrefix + "static_page.php")
    Call<ResponseBody> staticPage(
            @Query("type") String type
    );

    @GET(URLPrefix + "get_contact_detail.php")
    Call<ResponseBody> getContactDetail(
    );

    @GET(URLPrefix + "set_notification.php")
    Call<ResponseBody> setNotification(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token,
            @Query("status") String status
    );

    @GET(URLPrefix + "mark_news_read.php")
    Call<ResponseBody> markNewsRead(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token,
            @Query("news_id") String news_id
    );

    @GET(URLPrefix + "mark_news_bookmark.php")
    Call<ResponseBody> markBookMark(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token,
            @Query("news_id") String news_id
    );

    @GET(URLPrefix + "set_opinion_answer.php")
    Call<ResponseBody> opinionAnswer(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token,
            @Query("opinion_id") String opinion_id,
            @Query("ans_status") String ans_status
    );

    @GET(URLPrefix + "logout.php")
    Call<ResponseBody> logout(
            @Query("user_id") String user_id,
            @Query("auth_token") String auth_token
    );

}