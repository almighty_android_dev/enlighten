package com.enlighten.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.enlighten.controls.CustomAlertDialog;

public class Utils {

   static controls.CustomProgressDialog dialog;
    static CustomAlertDialog customAlertDialog;

    public static void showProgress(Activity activity) {
        dialog = new controls.CustomProgressDialog(activity);
        dialog.show();
    }

    public static void dismissProgress() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
        else
            Log.i("Dialog", "already dismissed");
    }

    public static void showCustomAlert(Activity activity,String message) {
        customAlertDialog = new CustomAlertDialog(activity,message);
        customAlertDialog.show();
    }

    public static void serverError(Activity activity, int code) {
       // dismissProgress();
        String message = "";
        switch (code) {
            case 400:
                message = "400 - Bad Request";
                break;
            case 401:
                message = "401 - Unauthorized";
                break;
            case 404:
                message = "404 - Not Found";
                break;
            case 500:
                message = "500 - Internal Server Error";
                break;
            default:
                message = "Server error";
        }
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    public static void internetAlert(Activity activity) {
        new AlertDialog.Builder(activity)
                .setMessage("Please check internet connection.")
                .setPositiveButton("Ok", null)
                .create()
                .show();
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static boolean isEmpty(View view) {
        if (view instanceof EditText) {
            if (((EditText) view).getText().toString().length() == 0) {
                return true;
            }
        } else if (view instanceof Button) {
            if (((Button) view).getText().toString().length() == 0) {
                return true;
            }
        }else if (view instanceof TextView) {
            if (((TextView) view).getText().toString().length() == 0) {
                return true;
            }
        }
        return false;
    }
}