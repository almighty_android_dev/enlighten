package com.enlighten.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import com.enlighten.R;
import com.enlighten.adapter.FeedPagerAdapterF;
import com.enlighten.adapter.OnSwipeTouchListener;
import com.enlighten.databinding.FragmentFeedBinding;
import com.enlighten.pojo.FeedPojo;
import com.enlighten.utils.Constants;
import com.enlighten.utils.RetrofitHelper;
import com.enlighten.utils.StoreUserData;
import com.enlighten.utils.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class FeedFragment extends Fragment {

    FragmentActivity activity;
    FragmentFeedBinding binding;
    StoreUserData storeUserData;
    private FeedFragment.ItemClickListener itemClickListener;
    TextToSpeech tts;
    int bookmark;
    FeedPagerAdapterF pagerAdapter;
    private InterstitialAd interstitialAd;
    Dialog dialog;
    FeedPojo data;
    int count , position;
    final ArrayList<FeedPojo.Responsedata> responsedata = new ArrayList<>();

    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = getActivity();
        binding = DataBindingUtil.inflate( inflater, R.layout.fragment_feed, container, false );
        storeUserData = new StoreUserData( activity );

        MobileAds.initialize( activity, initializationStatus -> {
        } );
        interstitialAd = new InterstitialAd( activity );
        interstitialAd.setAdUnitId( "ca-app-pub-4958310727087459/4348480957" );

        interstitialAd.setAdListener(
                new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        if (interstitialAd != null && interstitialAd.isLoaded()) {

                        } else {
                            if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                                AdRequest adRequest = new AdRequest.Builder().build();
                                interstitialAd.loadAd( adRequest );
                            }
                        }
                    }

                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        String error =
                                String.format(
                                        "domain: %s, code: %d, message: %s",
                                        loadAdError.getDomain(), loadAdError.getCode(), loadAdError.getMessage() );

                        Log.i( "Add_Fail", "onAdFailedToLoad: " + loadAdError );
                    }

                    @Override
                    public void onAdClosed() {
                        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                            AdRequest adRequest = new AdRequest.Builder().build();
                            interstitialAd.loadAd( adRequest );
                        }
                    }
                } );
        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder().build();
            interstitialAd.loadAd( adRequest );
        }

        binding.viewPagerFeed.setOnPageChangeListener( new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position % data.ad_count == 0 && data.ad_type.equalsIgnoreCase( "google" )) {
                    interstitialAd.show();
                }

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        } );

        tts = new TextToSpeech( activity.getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR) {
                    tts.setLanguage( Locale.US );
                }
            }
        } );

        getNews( "all" );

        itemClickListener = new ItemClickListener() {
            @Override
            public void onClick(String message, ImageView imageView) {
                tts.speak( message, TextToSpeech.QUEUE_FLUSH, null );
            }

            @Override
            public void llBookMark(int id) {
                bookmark = id;
                markBookMark();
            }

            @Override
            public void Refresh(String refresh) {
                getNews( "all" );
            }
        };

        pagerAdapter = new FeedPagerAdapterF( activity, responsedata, itemClickListener, "ALL"/*getIntent().getStringExtra( "TYPE" )*/ );
        binding.viewPagerFeed.setAdapter( pagerAdapter );

        binding.viewPagerFeed.addOnPageChangeListener( new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                position = position;
            }

            @Override
            public void onPageSelected(int position) {
                markNewsRead( "" + data.responsedata.get( position ).id );
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        } );

        binding.viewPagerFeed.setOnScrollChangeListener( new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                tts.stop();
            }
        } );

        return binding.getRoot();
    }

    public interface ItemClickListener {
        public void onClick(String message, ImageView imageView);

        public void llBookMark(int id);

        public void Refresh(String refresh);
    }

    public void onPause() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onPause();
    }

    private void getNews(String type) {
      // Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();

        Call<ResponseBody> call;

        call = retrofitHelper.api().getNews(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN ),
                type
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    assert body.body() != null;
                    String response = body.body().string();
                    Log.i( "GET_NEWS_RESPONSE", "" + response );

                    Reader reader = new StringReader( response );

                    Gson gson = new GsonBuilder()
                            .excludeFieldsWithModifiers( Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC )
                            .serializeNulls()
                            .create();

                    data = gson.fromJson( reader, FeedPojo.class );

                    if (data.status == 1) {
                        responsedata.clear();
                        responsedata.addAll( data.responsedata );
                        pagerAdapter = new FeedPagerAdapterF( activity, responsedata, itemClickListener, "all" );
                        binding.viewPagerFeed.setAdapter( pagerAdapter );
                        pagerAdapter.notifyDataSetChanged();

                        push_Counter();

                    } else {
                        //Utils.showCustomAlert( activity, data.message);
                        dialog = new Dialog( activity );
                        View providerDialog = getLayoutInflater().inflate( R.layout.custom_alert_dialog, null );
                        dialog.setContentView( providerDialog );
                        Objects.requireNonNull( dialog.getWindow() ).setLayout( LinearLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT );
                        dialog.getWindow().setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
                        dialog.show();

                        TextView tv = dialog.findViewById( R.id.message );
                        tv.setText( data.message );

                        dialog.findViewById( R.id.oKText ).setOnClickListener( new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                activity.finish();
                            }
                        } );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException e) {
                    e.printStackTrace();
                    Utils.dismissProgress();
                    Log.e( "EXCEPTION", ""+e );
                }
            }

            @Override
            public void onError(int code, String error) {
                Log.e( "EXCEPTIONERROR", error );
                Utils.dismissProgress();
            }
        } );
    }

    private void markBookMark() {
        //Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();

        Call<ResponseBody> call;

        call = retrofitHelper.api().markBookMark(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN ),
                "" + bookmark
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "BOOKMARK_RESPONSE", "" + response );

                    JSONObject jsonObject = new JSONObject( response );

                    if (jsonObject.getInt( "status" ) == 1) {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                        JSONObject data = jsonObject.getJSONObject( "responsedata" );


                    } else {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
                if (code == -1) {
                    Utils.showCustomAlert( activity, error );
                }
                Log.e( "Login Error", error );
            }
        } );
    }

    private void markNewsRead(String id) {
        RetrofitHelper retrofitHelper = new RetrofitHelper();

        Call<ResponseBody> call;

        call = retrofitHelper.api().markNewsRead(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN ),
                "" + id
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "MARK_READ", "" + response );


                } catch (IOException | NullPointerException | JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
                if (code == -1) {
                    Utils.showCustomAlert( activity, error );
                }
                Log.e( "Login Error", error );
            }
        } );
    }

    private void push_Counter() {
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;

        call = retrofitHelper.api().push_Counter(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN ),
                storeUserData.getString( Constants.USER_FCM )
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "PUSH_COUNTER_RESPONSE", "" + response );


                } catch (IOException | NullPointerException | JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {

            }
        } );
    }
}