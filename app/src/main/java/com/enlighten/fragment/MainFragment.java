package com.enlighten.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.enlighten.R;
import com.enlighten.activity.FeedbackActivity;
import com.enlighten.activity.InsightsActivity;
import com.enlighten.activity.LoginActivity;
import com.enlighten.activity.MotivationActivity;
import com.enlighten.activity.MyFeedActivity;
import com.enlighten.activity.MyOpinion;
import com.enlighten.adapter.SuggestedTopicAdapter;
import com.enlighten.databinding.FragmentMainBinding;
import com.enlighten.pojo.SuggestedTopicPojo;
import com.enlighten.utils.Constants;
import com.enlighten.utils.RetrofitHelper;
import com.enlighten.utils.StoreUserData;
import com.enlighten.utils.Utils;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Modifier;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class MainFragment extends Fragment {

    FragmentActivity activity;
    FragmentMainBinding binding;
    StoreUserData storeUserData;
    GoogleSignInClient mGoogleSignInClient;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = getActivity();
        storeUserData = new StoreUserData( activity );
        binding = DataBindingUtil.inflate( inflater, R.layout.fragment_main, container, false );

        if (!TextUtils.isEmpty( storeUserData.getString( Constants.NOTIFICATION ) ) && storeUserData.getString( Constants.NOTIFICATION ).equalsIgnoreCase( "1" )) {
            binding.switchNotification.setChecked( true );
        } else {
            binding.switchNotification.setChecked( false );
        }

        binding.llRateApp.setOnClickListener( new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                binding.drawerLayout.closeDrawer( Gravity.START );
                final String appPackageName = activity.getPackageName();
                try {
                    startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=" + appPackageName ) ) );
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "https://play.google.com/store/apps/details?id=" + appPackageName ) ) );
                }
            }
        } );

        binding.ivInstagram.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                binding.drawerLayout.closeDrawer( Gravity.START );
                Intent browserIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( "https://www.instagram.com/globalenlighten/" ) );
                startActivity( browserIntent );
            }
        } );
        binding.ivFaceBook.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.drawerLayout.closeDrawer( Gravity.START );
                Intent browserIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( "https://www.facebook.com/Enlighten-108389037702460/" ) );
                startActivity( browserIntent );
            }
        } );
        binding.ivTwitter.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.drawerLayout.closeDrawer( Gravity.START );
                Intent browserIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( "https://twitter.com/global_enlight" ) );
                startActivity( browserIntent );
            }
        } );

        binding.rvSuggestedTopic.setLayoutManager( new GridLayoutManager( activity, 3 ) );
        binding.rvSuggestedTopic.setNestedScrollingEnabled( true );
        binding.rvSuggestedTopic.setHasFixedSize( false );

        binding.llmotivation.setOnClickListener( v -> startActivity( new Intent( activity, MotivationActivity.class ) ) );
        binding.llopinion.setOnClickListener( v -> startActivity( new Intent( activity, MyOpinion.class ) ) );
        binding.contactus.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, FeedbackActivity.class )
                    .putExtra( "Activity", "contact" ) );
        } );
        binding.closeSetting.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
        } );
        binding.policy.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, FeedbackActivity.class )
                    .putExtra( "Activity", "policy" ) );
        } );
        binding.llterm.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, FeedbackActivity.class )
                    .putExtra( "Activity", "terms" ) );
        } );
        binding.feedback.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, FeedbackActivity.class )
                    .putExtra( "Activity", "feedback" ) );
        } );
        binding.llinsight.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, InsightsActivity.class ) );
        } );
        binding.llfeed.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, MyFeedActivity.class )
                    .putExtra( "TYPE", "MY_FEED" ) );
        } );
        binding.llallnews.setOnClickListener( v -> {

            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, MyFeedActivity.class )
                    .putExtra( "TYPE", "all" ) );
        } );
        binding.lltopstories.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, MyFeedActivity.class )
                    .putExtra( "TYPE", "top_story" ) );
        } );
        binding.lltrending.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, MyFeedActivity.class )
                    .putExtra( "TYPE", "trending" ) );
        } );
       /* binding.aeroMyFeed.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, MyFeedActivity.class )
                    .putExtra( "TYPE", "MY_FEED" ) );
        } );*/
        binding.llTrendingWorld.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, MyFeedActivity.class )
                    .putExtra( "TYPE", "trending" ) );
        } );
        binding.llbookmark.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, MyFeedActivity.class )
                    .putExtra( "TYPE", "bookmarked" ) );
        } );
        binding.llunread.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, MyFeedActivity.class )
                    .putExtra( "TYPE", "unread" ) );
        } );
        binding.bookmark.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, MyFeedActivity.class )
                    .putExtra( "TYPE", "bookmarked" ) );
        } );
        binding.shareapp.setOnClickListener( v -> {
            Intent shareIntent = new Intent( Intent.ACTION_SEND );
            shareIntent.setType( "text/plain" );
            shareIntent.putExtra( Intent.EXTRA_SUBJECT, "Enlighten" );
            String shareMessage = "\nEnlighten news app , Easy and quick way to read and listen the news, stay update all over the USA and worldwide in fingertips.\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + activity.getPackageName() + "\n\n";
            shareIntent.putExtra( Intent.EXTRA_TEXT, shareMessage );
            startActivity( Intent.createChooser( shareIntent, "choose one" ) );
        } );
        binding.btnSignIn.setOnClickListener( v -> {
            binding.drawerLayout.closeDrawer( Gravity.START );
            startActivity( new Intent( activity, LoginActivity.class ) );
        } );
        binding.settings.setOnClickListener( v -> {
            if (binding.drawerLayout.isDrawerOpen( Gravity.START )) {
                binding.drawerLayout.closeDrawer( Gravity.START );
            } else {
                binding.drawerLayout.openDrawer( Gravity.START );
            }
        } );
        binding.switchNotification.setOnCheckedChangeListener( (buttonView, isChecked) -> {
            if (isChecked) {
                setNotification( "1" );
            } else {
                setNotification( "0" );
            }

        } );
        binding.btnLogout.setOnClickListener( v -> {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder( GoogleSignInOptions.DEFAULT_SIGN_IN ).requestEmail().build();
            mGoogleSignInClient = GoogleSignIn.getClient( activity, gso );
            mGoogleSignInClient.signOut()
                    .addOnCompleteListener( activity, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            binding.drawerLayout.closeDrawer( Gravity.START );
                            logout();
                        }
                    } );
        } );

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.tvUserName.setText( storeUserData.getString( Constants.USER_NAME ) );
        Glide.with( activity )
                .load( storeUserData.getString( Constants.USER_IMAGE ) )
                .circleCrop().into( binding.ivUserImage );
        getTopic();
        if (!TextUtils.isEmpty( storeUserData.getString( Constants.LOGIN_WITH_GOOGLE ) )) {
            binding.layoutlogout.setVisibility( View.VISIBLE );
            binding.layoutSignin.setVisibility( View.GONE );
        } else {
            binding.layoutSignin.setVisibility( View.VISIBLE );
        }
    }

    private void setNotification(String status) {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;

        call = retrofitHelper.api().setNotification(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN ),
                status
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "NOTIFICATION_RESPONSE", "" + response );

                    JSONObject jsonObject = new JSONObject( response );

                    if (jsonObject.getInt( "status" ) == 1) {
                        storeUserData.setString( Constants.NOTIFICATION, status );
                    } else {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                    }
                } catch (IOException | NullPointerException | JsonSyntaxException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();
            }
        } );
    }

    private void logout() {
        Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;

        call = retrofitHelper.api().logout(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN )
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "LOGOUT_RESPONSE", "" + response );


                    JSONObject jsonObject = new JSONObject( response );

                    if (jsonObject.getInt( "status" ) == 1) {
                        storeUserData.clearData( activity );
                        startActivity( new Intent( activity, LoginActivity.class ) );

                    } else {
                        Utils.showCustomAlert( activity, jsonObject.getString( "message" ) );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {

            }
        } );
    }

    private void getTopic() {
       // Utils.showProgress( activity );
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;

        call = retrofitHelper.api().getTopic(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN )
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                Utils.dismissProgress();
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "SUGGESTED_TOPICS", "" + response );

                    Reader reader = new StringReader( response );

                    Gson gson = new GsonBuilder()
                            .excludeFieldsWithModifiers( Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC )
                            .serializeNulls()
                            .create();

                    //pojo = gson.fromJson( reader, SuggestedTopicPojo.class );

                    SuggestedTopicPojo data = gson.fromJson( reader, SuggestedTopicPojo.class );

                    if (data.status == 1) {
                        binding.ivInstagram.setOnClickListener( new View.OnClickListener() {
                            @SuppressLint("WrongConstant")
                            @Override
                            public void onClick(View v) {
                                binding.drawerLayout.closeDrawer( Gravity.START );
                                Intent browserIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( data.responsedata.links.insta_url ) );
                                startActivity( browserIntent );
                            }
                        } );
                        binding.ivFaceBook.setOnClickListener( new View.OnClickListener() {
                            @SuppressLint("WrongConstant")
                            @Override
                            public void onClick(View v) {
                                binding.drawerLayout.closeDrawer( Gravity.START );
                                Intent browserIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( data.responsedata.links.fb_url ) );
                                startActivity( browserIntent );
                            }
                        } );
                        binding.ivTwitter.setOnClickListener( new View.OnClickListener() {
                            @SuppressLint("WrongConstant")
                            @Override
                            public void onClick(View v) {
                                binding.drawerLayout.closeDrawer( Gravity.START );
                                Intent browserIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( data.responsedata.links.twitter_url ) );
                                startActivity( browserIntent );
                            }
                        } );
                        binding.rvSuggestedTopic.setAdapter( new SuggestedTopicAdapter( activity, data.responsedata.topics ) );
                        push_Counter();


                    } else {
                        Utils.showCustomAlert( activity, data.message );
                    }

                } catch (IOException | NullPointerException | JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
                Utils.dismissProgress();

            }
        } );
    }

    private void push_Counter() {
        RetrofitHelper retrofitHelper = new RetrofitHelper();
        Call<ResponseBody> call;

        call = retrofitHelper.api().push_Counter(
                storeUserData.getString( Constants.USER_ID ),
                storeUserData.getString( Constants.TOKEN ),
                storeUserData.getString( Constants.USER_FCM )
        );

        retrofitHelper.callApi( activity, call, new RetrofitHelper.ConnectionCallBack() {
            @Override
            public void onSuccess(Response<ResponseBody> body) {
                try {
                    if (body.code() != 200) {
                        Utils.serverError( activity, body.code() );
                        return;
                    }
                    String response = body.body().string();
                    Log.i( "PUSH_COUNTER_RESPONSE", "" + response );


                } catch (IOException | NullPointerException | JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {

            }
        } );
    }
}