package com.enlighten.databinding;
import com.enlighten.R;
import com.enlighten.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMainBindingImpl extends FragmentMainBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.drawer_layout, 1);
        sViewsWithIds.put(R.id.settings, 2);
        sViewsWithIds.put(R.id.llopinion, 3);
        sViewsWithIds.put(R.id.llinsight, 4);
        sViewsWithIds.put(R.id.llmotivation, 5);
        sViewsWithIds.put(R.id.llTrendingWorld, 6);
        sViewsWithIds.put(R.id.llfeed, 7);
        sViewsWithIds.put(R.id.llallnews, 8);
        sViewsWithIds.put(R.id.lltopstories, 9);
        sViewsWithIds.put(R.id.lltrending, 10);
        sViewsWithIds.put(R.id.llbookmark, 11);
        sViewsWithIds.put(R.id.llunread, 12);
        sViewsWithIds.put(R.id.rvSuggestedTopic, 13);
        sViewsWithIds.put(R.id.nav_view, 14);
        sViewsWithIds.put(R.id.layoutSignin, 15);
        sViewsWithIds.put(R.id.llsignin, 16);
        sViewsWithIds.put(R.id.btnSignIn, 17);
        sViewsWithIds.put(R.id.closeSetting, 18);
        sViewsWithIds.put(R.id.layoutlogout, 19);
        sViewsWithIds.put(R.id.ivUserImage, 20);
        sViewsWithIds.put(R.id.tvUserName, 21);
        sViewsWithIds.put(R.id.btnLogout, 22);
        sViewsWithIds.put(R.id.switchNotification, 23);
        sViewsWithIds.put(R.id.shareapp, 24);
        sViewsWithIds.put(R.id.bookmark, 25);
        sViewsWithIds.put(R.id.llRateApp, 26);
        sViewsWithIds.put(R.id.feedback, 27);
        sViewsWithIds.put(R.id.llterm, 28);
        sViewsWithIds.put(R.id.policy, 29);
        sViewsWithIds.put(R.id.contactus, 30);
        sViewsWithIds.put(R.id.ivInstagram, 31);
        sViewsWithIds.put(R.id.ivFaceBook, 32);
        sViewsWithIds.put(R.id.ivTwitter, 33);
    }
    // views
    @NonNull
    private final androidx.coordinatorlayout.widget.CoordinatorLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMainBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 34, sIncludes, sViewsWithIds));
    }
    private FragmentMainBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[25]
            , (androidx.cardview.widget.CardView) bindings[22]
            , (androidx.cardview.widget.CardView) bindings[17]
            , (android.widget.ImageView) bindings[18]
            , (android.widget.LinearLayout) bindings[30]
            , (androidx.drawerlayout.widget.DrawerLayout) bindings[1]
            , (android.widget.LinearLayout) bindings[27]
            , (android.widget.ImageView) bindings[32]
            , (android.widget.ImageView) bindings[31]
            , (android.widget.ImageView) bindings[33]
            , (android.widget.ImageView) bindings[20]
            , (android.widget.LinearLayout) bindings[15]
            , (android.widget.LinearLayout) bindings[19]
            , (android.widget.LinearLayout) bindings[26]
            , (android.widget.LinearLayout) bindings[6]
            , (android.widget.LinearLayout) bindings[8]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.LinearLayout) bindings[7]
            , (android.widget.LinearLayout) bindings[4]
            , (android.widget.LinearLayout) bindings[5]
            , (android.widget.LinearLayout) bindings[3]
            , (android.widget.LinearLayout) bindings[16]
            , (android.widget.LinearLayout) bindings[28]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.LinearLayout) bindings[10]
            , (android.widget.LinearLayout) bindings[12]
            , (com.google.android.material.navigation.NavigationView) bindings[14]
            , (android.widget.LinearLayout) bindings[29]
            , (androidx.recyclerview.widget.RecyclerView) bindings[13]
            , (android.widget.ImageView) bindings[2]
            , (android.widget.LinearLayout) bindings[24]
            , (android.widget.Switch) bindings[23]
            , (controls.CTextView) bindings[21]
            );
        this.mboundView0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}