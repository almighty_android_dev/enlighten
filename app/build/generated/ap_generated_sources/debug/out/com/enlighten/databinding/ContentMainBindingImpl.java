package com.enlighten.databinding;
import com.enlighten.R;
import com.enlighten.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ContentMainBindingImpl extends ContentMainBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.llMainLayout, 1);
        sViewsWithIds.put(R.id.imageViewFeed, 2);
        sViewsWithIds.put(R.id.youtubePlayerView, 3);
        sViewsWithIds.put(R.id.llTopLayout, 4);
        sViewsWithIds.put(R.id.imgMenu, 5);
        sViewsWithIds.put(R.id.llMenu, 6);
        sViewsWithIds.put(R.id.llShareBtn, 7);
        sViewsWithIds.put(R.id.tvShare, 8);
        sViewsWithIds.put(R.id.llBookmark, 9);
        sViewsWithIds.put(R.id.tvBookmark, 10);
        sViewsWithIds.put(R.id.llRefreshBtn, 11);
        sViewsWithIds.put(R.id.tvRefresh, 12);
        sViewsWithIds.put(R.id.textToSpeechFeed, 13);
        sViewsWithIds.put(R.id.tvTitleFeed, 14);
        sViewsWithIds.put(R.id.tvContentFeed, 15);
        sViewsWithIds.put(R.id.llKnowMore, 16);
        sViewsWithIds.put(R.id.tvBottomLineFeed, 17);
        sViewsWithIds.put(R.id.ivAddFeed, 18);
        sViewsWithIds.put(R.id.llVideoAds, 19);
        sViewsWithIds.put(R.id.videoAds, 20);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ContentMainBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private ContentMainBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.FrameLayout) bindings[0]
            , (android.widget.ImageView) bindings[2]
            , (android.widget.ImageView) bindings[5]
            , (android.widget.ImageView) bindings[18]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.LinearLayout) bindings[16]
            , (android.widget.LinearLayout) bindings[1]
            , (android.widget.LinearLayout) bindings[6]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.LinearLayout) bindings[7]
            , (android.widget.LinearLayout) bindings[4]
            , (android.widget.LinearLayout) bindings[19]
            , (android.widget.ImageView) bindings[13]
            , (controls.CTextView) bindings[10]
            , (controls.CTextView) bindings[17]
            , (controls.CTextView) bindings[15]
            , (controls.CTextView) bindings[12]
            , (controls.CTextView) bindings[8]
            , (controls.CTextView) bindings[14]
            , (com.potyvideo.library.AndExoPlayerView) bindings[20]
            , (com.jaedongchicken.ytplayer.YoutubePlayerView) bindings[3]
            );
        this.frameLayoutMyFeed.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}