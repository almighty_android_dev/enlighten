package com.enlighten.databinding;
import com.enlighten.R;
import com.enlighten.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class OpinionSliderBindingImpl extends OpinionSliderBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.llNewsOpinion, 1);
        sViewsWithIds.put(R.id.youtubePlayerViewOpinion, 2);
        sViewsWithIds.put(R.id.imageViewOpinion, 3);
        sViewsWithIds.put(R.id.ivShareOpinion, 4);
        sViewsWithIds.put(R.id.textToSpeechO, 5);
        sViewsWithIds.put(R.id.tvTitleOpinion, 6);
        sViewsWithIds.put(R.id.tvContent, 7);
        sViewsWithIds.put(R.id.tvPollQuestion, 8);
        sViewsWithIds.put(R.id.rating, 9);
        sViewsWithIds.put(R.id.btnYes, 10);
        sViewsWithIds.put(R.id.btnNo, 11);
        sViewsWithIds.put(R.id.rating2, 12);
        sViewsWithIds.put(R.id.activeProgress, 13);
        sViewsWithIds.put(R.id.tvYesPer, 14);
        sViewsWithIds.put(R.id.perYes, 15);
        sViewsWithIds.put(R.id.tvNoPer, 16);
        sViewsWithIds.put(R.id.perNo, 17);
        sViewsWithIds.put(R.id.tvBottemLine, 18);
        sViewsWithIds.put(R.id.ivAddOpinion, 19);
        sViewsWithIds.put(R.id.llVideoOpinion, 20);
        sViewsWithIds.put(R.id.videoOpinionAdd, 21);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public OpinionSliderBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 22, sIncludes, sViewsWithIds));
    }
    private OpinionSliderBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ProgressBar) bindings[13]
            , (controls.CTextView) bindings[11]
            , (controls.CTextView) bindings[10]
            , (android.widget.FrameLayout) bindings[0]
            , (android.widget.ImageView) bindings[3]
            , (android.widget.ImageView) bindings[19]
            , (android.widget.ImageView) bindings[4]
            , (android.widget.LinearLayout) bindings[1]
            , (android.widget.LinearLayout) bindings[20]
            , (controls.CTextView) bindings[17]
            , (controls.CTextView) bindings[15]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.FrameLayout) bindings[12]
            , (android.widget.ImageView) bindings[5]
            , (controls.CTextView) bindings[18]
            , (controls.CTextView) bindings[7]
            , (controls.CTextView) bindings[16]
            , (controls.CTextView) bindings[8]
            , (controls.CTextView) bindings[6]
            , (controls.CTextView) bindings[14]
            , (com.potyvideo.library.AndExoPlayerView) bindings[21]
            , (com.jaedongchicken.ytplayer.YoutubePlayerView) bindings[2]
            );
        this.frameLayoutOpinion.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}