package com.enlighten;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.enlighten.databinding.ActivityDemoScroll3BindingImpl;
import com.enlighten.databinding.ActivityFeedbackBindingImpl;
import com.enlighten.databinding.ActivityHomeBindingImpl;
import com.enlighten.databinding.ActivityInsightsBindingImpl;
import com.enlighten.databinding.ActivityLoginBindingImpl;
import com.enlighten.databinding.ActivityMyFeedBindingImpl;
import com.enlighten.databinding.ActivityMyOpinionBindingImpl;
import com.enlighten.databinding.ActivitySplashBindingImpl;
import com.enlighten.databinding.ContentMainBindingImpl;
import com.enlighten.databinding.FragmentFeedBindingImpl;
import com.enlighten.databinding.FragmentMainBindingImpl;
import com.enlighten.databinding.MotivationSliderBindingImpl;
import com.enlighten.databinding.OpinionSliderBindingImpl;
import com.enlighten.databinding.SuggestedTopicRowBindingImpl;
import com.enlighten.databinding.TestBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYDEMOSCROLL3 = 1;

  private static final int LAYOUT_ACTIVITYFEEDBACK = 2;

  private static final int LAYOUT_ACTIVITYHOME = 3;

  private static final int LAYOUT_ACTIVITYINSIGHTS = 4;

  private static final int LAYOUT_ACTIVITYLOGIN = 5;

  private static final int LAYOUT_ACTIVITYMYFEED = 6;

  private static final int LAYOUT_ACTIVITYMYOPINION = 7;

  private static final int LAYOUT_ACTIVITYSPLASH = 8;

  private static final int LAYOUT_CONTENTMAIN = 9;

  private static final int LAYOUT_FRAGMENTFEED = 10;

  private static final int LAYOUT_FRAGMENTMAIN = 11;

  private static final int LAYOUT_MOTIVATIONSLIDER = 12;

  private static final int LAYOUT_OPINIONSLIDER = 13;

  private static final int LAYOUT_SUGGESTEDTOPICROW = 14;

  private static final int LAYOUT_TEST = 15;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(15);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.activity_demo_scroll3, LAYOUT_ACTIVITYDEMOSCROLL3);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.activity_feedback, LAYOUT_ACTIVITYFEEDBACK);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.activity_home, LAYOUT_ACTIVITYHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.activity_insights, LAYOUT_ACTIVITYINSIGHTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.activity_login, LAYOUT_ACTIVITYLOGIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.activity_my_feed, LAYOUT_ACTIVITYMYFEED);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.activity_my_opinion, LAYOUT_ACTIVITYMYOPINION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.activity_splash, LAYOUT_ACTIVITYSPLASH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.content_main, LAYOUT_CONTENTMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.fragment_feed, LAYOUT_FRAGMENTFEED);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.fragment_main, LAYOUT_FRAGMENTMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.motivation_slider, LAYOUT_MOTIVATIONSLIDER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.opinion_slider, LAYOUT_OPINIONSLIDER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.suggested_topic_row, LAYOUT_SUGGESTEDTOPICROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.enlighten.R.layout.test, LAYOUT_TEST);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYDEMOSCROLL3: {
          if ("layout/activity_demo_scroll3_0".equals(tag)) {
            return new ActivityDemoScroll3BindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_demo_scroll3 is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYFEEDBACK: {
          if ("layout/activity_feedback_0".equals(tag)) {
            return new ActivityFeedbackBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_feedback is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYHOME: {
          if ("layout/activity_home_0".equals(tag)) {
            return new ActivityHomeBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_home is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYINSIGHTS: {
          if ("layout/activity_insights_0".equals(tag)) {
            return new ActivityInsightsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_insights is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYLOGIN: {
          if ("layout/activity_login_0".equals(tag)) {
            return new ActivityLoginBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_login is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYMYFEED: {
          if ("layout/activity_my_feed_0".equals(tag)) {
            return new ActivityMyFeedBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_my_feed is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYMYOPINION: {
          if ("layout/activity_my_opinion_0".equals(tag)) {
            return new ActivityMyOpinionBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_my_opinion is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSPLASH: {
          if ("layout/activity_splash_0".equals(tag)) {
            return new ActivitySplashBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_splash is invalid. Received: " + tag);
        }
        case  LAYOUT_CONTENTMAIN: {
          if ("layout/content_main_0".equals(tag)) {
            return new ContentMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for content_main is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTFEED: {
          if ("layout/fragment_feed_0".equals(tag)) {
            return new FragmentFeedBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_feed is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTMAIN: {
          if ("layout/fragment_main_0".equals(tag)) {
            return new FragmentMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_main is invalid. Received: " + tag);
        }
        case  LAYOUT_MOTIVATIONSLIDER: {
          if ("layout/motivation_slider_0".equals(tag)) {
            return new MotivationSliderBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for motivation_slider is invalid. Received: " + tag);
        }
        case  LAYOUT_OPINIONSLIDER: {
          if ("layout/opinion_slider_0".equals(tag)) {
            return new OpinionSliderBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for opinion_slider is invalid. Received: " + tag);
        }
        case  LAYOUT_SUGGESTEDTOPICROW: {
          if ("layout/suggested_topic_row_0".equals(tag)) {
            return new SuggestedTopicRowBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for suggested_topic_row is invalid. Received: " + tag);
        }
        case  LAYOUT_TEST: {
          if ("layout/test_0".equals(tag)) {
            return new TestBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for test is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(1);

    static {
      sKeys.put(0, "_all");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(15);

    static {
      sKeys.put("layout/activity_demo_scroll3_0", com.enlighten.R.layout.activity_demo_scroll3);
      sKeys.put("layout/activity_feedback_0", com.enlighten.R.layout.activity_feedback);
      sKeys.put("layout/activity_home_0", com.enlighten.R.layout.activity_home);
      sKeys.put("layout/activity_insights_0", com.enlighten.R.layout.activity_insights);
      sKeys.put("layout/activity_login_0", com.enlighten.R.layout.activity_login);
      sKeys.put("layout/activity_my_feed_0", com.enlighten.R.layout.activity_my_feed);
      sKeys.put("layout/activity_my_opinion_0", com.enlighten.R.layout.activity_my_opinion);
      sKeys.put("layout/activity_splash_0", com.enlighten.R.layout.activity_splash);
      sKeys.put("layout/content_main_0", com.enlighten.R.layout.content_main);
      sKeys.put("layout/fragment_feed_0", com.enlighten.R.layout.fragment_feed);
      sKeys.put("layout/fragment_main_0", com.enlighten.R.layout.fragment_main);
      sKeys.put("layout/motivation_slider_0", com.enlighten.R.layout.motivation_slider);
      sKeys.put("layout/opinion_slider_0", com.enlighten.R.layout.opinion_slider);
      sKeys.put("layout/suggested_topic_row_0", com.enlighten.R.layout.suggested_topic_row);
      sKeys.put("layout/test_0", com.enlighten.R.layout.test);
    }
  }
}
