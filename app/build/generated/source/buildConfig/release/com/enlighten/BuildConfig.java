/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.enlighten;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.enlighten";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 10;
  public static final String VERSION_NAME = "1.3";
}
