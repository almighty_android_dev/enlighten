// Generated by data binding compiler. Do not edit!
package com.enlighten.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.enlighten.R;
import com.jaedongchicken.ytplayer.YoutubePlayerView;
import com.potyvideo.library.AndExoPlayerView;
import controls.CTextView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class OpinionSliderBinding extends ViewDataBinding {
  @NonNull
  public final ProgressBar activeProgress;

  @NonNull
  public final CTextView btnNo;

  @NonNull
  public final CTextView btnYes;

  @NonNull
  public final FrameLayout frameLayoutOpinion;

  @NonNull
  public final ImageView imageViewOpinion;

  @NonNull
  public final ImageView ivAddOpinion;

  @NonNull
  public final ImageView ivShareOpinion;

  @NonNull
  public final LinearLayout llNewsOpinion;

  @NonNull
  public final LinearLayout llVideoOpinion;

  @NonNull
  public final CTextView perNo;

  @NonNull
  public final CTextView perYes;

  @NonNull
  public final LinearLayout rating;

  @NonNull
  public final FrameLayout rating2;

  @NonNull
  public final ImageView textToSpeechO;

  @NonNull
  public final CTextView tvBottemLine;

  @NonNull
  public final CTextView tvContent;

  @NonNull
  public final CTextView tvNoPer;

  @NonNull
  public final CTextView tvPollQuestion;

  @NonNull
  public final CTextView tvTitleOpinion;

  @NonNull
  public final CTextView tvYesPer;

  @NonNull
  public final AndExoPlayerView videoOpinionAdd;

  @NonNull
  public final YoutubePlayerView youtubePlayerViewOpinion;

  protected OpinionSliderBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ProgressBar activeProgress, CTextView btnNo, CTextView btnYes, FrameLayout frameLayoutOpinion,
      ImageView imageViewOpinion, ImageView ivAddOpinion, ImageView ivShareOpinion,
      LinearLayout llNewsOpinion, LinearLayout llVideoOpinion, CTextView perNo, CTextView perYes,
      LinearLayout rating, FrameLayout rating2, ImageView textToSpeechO, CTextView tvBottemLine,
      CTextView tvContent, CTextView tvNoPer, CTextView tvPollQuestion, CTextView tvTitleOpinion,
      CTextView tvYesPer, AndExoPlayerView videoOpinionAdd,
      YoutubePlayerView youtubePlayerViewOpinion) {
    super(_bindingComponent, _root, _localFieldCount);
    this.activeProgress = activeProgress;
    this.btnNo = btnNo;
    this.btnYes = btnYes;
    this.frameLayoutOpinion = frameLayoutOpinion;
    this.imageViewOpinion = imageViewOpinion;
    this.ivAddOpinion = ivAddOpinion;
    this.ivShareOpinion = ivShareOpinion;
    this.llNewsOpinion = llNewsOpinion;
    this.llVideoOpinion = llVideoOpinion;
    this.perNo = perNo;
    this.perYes = perYes;
    this.rating = rating;
    this.rating2 = rating2;
    this.textToSpeechO = textToSpeechO;
    this.tvBottemLine = tvBottemLine;
    this.tvContent = tvContent;
    this.tvNoPer = tvNoPer;
    this.tvPollQuestion = tvPollQuestion;
    this.tvTitleOpinion = tvTitleOpinion;
    this.tvYesPer = tvYesPer;
    this.videoOpinionAdd = videoOpinionAdd;
    this.youtubePlayerViewOpinion = youtubePlayerViewOpinion;
  }

  @NonNull
  public static OpinionSliderBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.opinion_slider, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static OpinionSliderBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<OpinionSliderBinding>inflateInternal(inflater, R.layout.opinion_slider, root, attachToRoot, component);
  }

  @NonNull
  public static OpinionSliderBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.opinion_slider, null, false, component)
   */
  @NonNull
  @Deprecated
  public static OpinionSliderBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<OpinionSliderBinding>inflateInternal(inflater, R.layout.opinion_slider, null, false, component);
  }

  public static OpinionSliderBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static OpinionSliderBinding bind(@NonNull View view, @Nullable Object component) {
    return (OpinionSliderBinding)bind(component, view, R.layout.opinion_slider);
  }
}
